/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.DAOFactory;
import dao.PostDAO;
import dao.UsuarioDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;
import model.Post;
import model.PostcomLike;
import model.Usuario;

/**
 *
 * @author Kelvin
 */
@WebServlet(name = "WelcomeController", urlPatterns = {""})
public class WelcomeController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UsuarioDAO dao;
        PostDAO daozinho;
        Usuario usuario = null;
        HttpSession teste = request.getSession(false);

        if (teste != null) {
            Integer id = (Integer) teste.getAttribute("usuario");
            if (!(id == null)) {
                try (DAOFactory daoFactory = new DAOFactory()) {
                    dao = daoFactory.getUsuarioDAO();

                    usuario = dao.read(id);

                } catch (SQLException | SecurityException ex) {
                    System.err.println(ex.getMessage());
                }

            }
        }
        if (usuario == null) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/view/login.jsp");
            dispatcher.forward(request, response);

        } else {
            request.setAttribute("usuario", usuario);
       
            try (DAOFactory daoFactory = new DAOFactory()) {
                daozinho = daoFactory.getPostDAO();
                List<Post> posterinos = new ArrayList<Post>();
                List<Post> comarroba = new ArrayList();
                List<Post> reposts = daozinho.listaRepostagem(usuario.getId());
                
                
                Integer retorno = daozinho.zona_influencia(usuario.getId());
                request.setAttribute("retorno", retorno);
                
                
                 

                     
                posterinos = daozinho.getTodosPosts(usuario.getId());
                 for(Post i : reposts)
                 {
                     //System.out.println(i.getTexto());
                     posterinos.add(i);
                
                 }
                 for (Post p : posterinos) {
                            p = daozinho.retornaPostcomTag(p, usuario, (Integer) teste.getAttribute("usuario"));

                            comarroba.add(p);

                        }

                request.setAttribute("posts", comarroba);
                
                List<PostcomLike> liked = new ArrayList();

                for (Post i : posterinos) {

                    Integer a = daozinho.qtdLike(i.getId_post());
                    Integer b = daozinho.qtdDislike(i.getId_post());
                    PostcomLike testerino = new PostcomLike();

                    testerino.setPost(i);
                    // System.out.println(a);
                    testerino.setQtd_Like(a);
                    testerino.setQtd_Dislike(b);

                    liked.add(testerino);
                }
                request.setAttribute("posts", liked);
               
            } catch (SQLException | SecurityException ex) {
                System.err.println(ex.getMessage());
            }
            RequestDispatcher dispatcher = request.getRequestDispatcher("/view/welcome.jsp");
            dispatcher.forward(request, response);
        }

    }

}
