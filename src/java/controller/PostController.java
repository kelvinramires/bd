package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dao.DAO;
import dao.DAOFactory;
import dao.PostDAO;
import dao.UsuarioDAO;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Post;
import model.PostcomLike;
import model.Usuario;
import sun.security.pkcs.PKCS9Attribute;

@WebServlet(urlPatterns = {"/post/create",
    "/post/read",
    "/post/update",
    "/post/delete",
    "/post/timeline",
    "/post/comentario",
    "/post/apagacoment",
    "/post/comentariomeus",
    "/post/posthashtag",
    "/post/repost",
    "/post/like",
    "/post/estatisticas",
    "/post/top20",
    "/post/top3",
    "/post/topusu",
    "/post/similaridade",
    "/post/busca",
    "/post/dislike",
    "/post/unlike",
    "/post/undislike",
    "/post"})

public class PostController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("autenticado", request.getSession().getAttribute("usuario"));
        DAO dao;
        PostDAO daozinho;
        UsuarioDAO daozao;
        Usuario usuario = new Usuario();
        Post post;
        RequestDispatcher dispatcher;
        HttpSession session;

        switch (request.getServletPath()) {
            case "/post/create":
                dispatcher = request.getRequestDispatcher("/view/post/create.jsp");
                dispatcher.forward(request, response);
                break;

            case "/post/read":
                session = request.getSession(false);

                if (session != null) {
                    Integer id = (Integer) session.getAttribute("usuario");
                    if (!(id == null)) {
                        try (DAOFactory daoFactory = new DAOFactory()) {
                            daozao = daoFactory.getUsuarioDAO();

                            usuario = daozao.read(id);

                        } catch (SQLException | SecurityException ex) {
                            System.err.println(ex.getMessage());
                        }

                    }
                }
                if (usuario == null) {
                    RequestDispatcher dispatchers = request.getRequestDispatcher("/view/login.jsp");
                    dispatchers.forward(request, response);

                } else {
                    request.setAttribute("usuario", usuario);

                    try (DAOFactory daoFactory = new DAOFactory();) {
                        daozinho = daoFactory.getPostDAO();
                        /* String i = request.getParameter("id_usuario");
                         System.out.println(i);
                         int b = Integer.parseInt(request.getParameter("id_usuario"));
                         System.out.println("o valor de b eh =>" + b); */

                        post = (Post) daozinho.read(Integer.parseInt(request.getParameter("id_post")));
                        post.setId_usuario(usuario.getId());
                        post = daozinho.retornaPostcomTag(post, usuario, (Integer) session.getAttribute("usuario"));

                        /* System.out.println("Estoy aqui"); */
                        request.setAttribute("post", post);

                        dispatcher = request.getRequestDispatcher("/view" + request.getServletPath() + ".jsp");
                        dispatcher.forward(request, response);
                    } catch (SQLException ex) {
                        System.err.println(ex.getMessage());
                        response.sendRedirect(request.getContextPath() + "/post");
                    }
                }

                break;
            case "/post/estatisticas":
                dispatcher = request.getRequestDispatcher("/view/post/estatisticas.jsp");
                dispatcher.forward(request, response);
                break;

            case "/post/update":
                session = request.getSession(false);

                if (session != null) {
                    Integer id = (Integer) session.getAttribute("usuario");
                    if (!(id == null)) {
                        try (DAOFactory daoFactory = new DAOFactory()) {
                            daozao = daoFactory.getUsuarioDAO();

                            usuario = daozao.read(id);

                        } catch (SQLException | SecurityException ex) {
                            System.err.println(ex.getMessage());
                        }

                    }
                }
                if (usuario == null) {
                    RequestDispatcher dispatchers = request.getRequestDispatcher("/view/login.jsp");
                    dispatchers.forward(request, response);

                } else {
                    request.setAttribute("usuario", usuario);

                    try (DAOFactory daoFactory = new DAOFactory();) {
                        daozinho = daoFactory.getPostDAO();
                        /* String i = request.getParameter("id_usuario");
                         System.out.println(i);
                         int b = Integer.parseInt(request.getParameter("id_usuario"));
                         System.out.println("o valor de b eh =>" + b); */

                        post = (Post) daozinho.read(Integer.parseInt(request.getParameter("id_post")));
                        post.setId_usuario(usuario.getId());
                        //post = daozinho.retornaPostcomArroba(post, usuario, (Integer) session.getAttribute("usuario"));

                        /* System.out.println("Estoy aqui"); */
                        request.setAttribute("post", post);

                        dispatcher = request.getRequestDispatcher("/view" + request.getServletPath() + ".jsp");
                        dispatcher.forward(request, response);
                    } catch (SQLException ex) {
                        System.err.println(ex.getMessage());
                        response.sendRedirect(request.getContextPath() + "/post");
                    }
                }

                break;

            case "/post/delete":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getPostDAO();

                    dao.delete(Integer.parseInt(request.getParameter("id_post")));
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }

                response.sendRedirect(request.getContextPath() + "/post");
                break;
            case "/post/timeline":
                session = request.getSession(false);

                if (session != null) {
                    Integer id = (Integer) session.getAttribute("usuario");
                    if (!(id == null)) {
                        try (DAOFactory daoFactory = new DAOFactory()) {
                            daozao = daoFactory.getUsuarioDAO();

                            usuario = daozao.read(id);

                        } catch (SQLException | SecurityException ex) {
                            System.err.println(ex.getMessage());
                        }

                    }
                }
                if (usuario == null) {
                    RequestDispatcher dispatchers = request.getRequestDispatcher("/view/login.jsp");
                    dispatchers.forward(request, response);

                } else {
                    request.setAttribute("usuario", usuario);
                    try (DAOFactory daoFactory = new DAOFactory();) {
                        daozinho = daoFactory.getPostDAO();

                        List<Post> seguidoresPosts = daozinho.getPostsSeguidores(Integer.parseInt(request.getParameter("id")));
                        List<Post> reposts = new LinkedList();
                        for (Post k : seguidoresPosts) {
                            // System.out.println(k.getId_usuario());
                            reposts = daozinho.listaRepostagem(k.getId_usuario());
                        }

                        for (Post i : reposts) {
                            //System.out.println(i.getTexto());
                            seguidoresPosts.add(i);

                        }
                        List<Post> comarroba = new ArrayList();
                        for (Post p : seguidoresPosts) {
                            p = daozinho.retornaPostcomTag(p, usuario, (Integer) session.getAttribute("usuario"));

                            comarroba.add(p);

                        }
                        /*for (Post o : seguidoresPosts) {
                         System.out.println(o.getNome());

                         }*/

                        //request.setAttribute("seguidoresPosts", seguidoresPosts);
                        List<PostcomLike> liked = new ArrayList();

                        for (Post i : comarroba) {

                            boolean like = daozinho.verificaLike(Integer.parseInt(request.getParameter("id")), i.getId_post());
                            boolean dislike = daozinho.verificaDislike(Integer.parseInt(request.getParameter("id")), i.getId_post());
                            Integer a = daozinho.qtdLike(i.getId_post());
                            Integer b = daozinho.qtdDislike(i.getId_post());
                            PostcomLike teste = new PostcomLike();

                            teste.setPost(i);
                            // System.out.println(a);
                            teste.setQtd_Like(a);
                            teste.setQtd_Dislike(b);
                            teste.setLike(like);
                            teste.setDislike(dislike);
                            liked.add(teste);
                        }
                        request.setAttribute("seguidoresPosts", liked);

                        dispatcher = request.getRequestDispatcher("/view/post/timeline.jsp");
                        dispatcher.forward(request, response);
                    } catch (SQLException ex) {
                        System.err.println(ex.getMessage());
                        response.sendRedirect(request.getContextPath() + "/");
                    }
                }
                break;
            case "/post/busca":
                String busca = request.getParameter("busca");
                String[] separado = busca.split(" ");
                List<String> arrobas = new ArrayList();
                List<String> hashtags = new ArrayList();

                for (String valor : separado) {
                    if (valor.startsWith("@")) {
                        arrobas.add(valor.substring(1));

                    } else if (valor.startsWith("#")) {
                        hashtags.add(valor.substring(1));

                    }

                }
                session = request.getSession(false);

                if (session != null) {
                    Integer id = (Integer) session.getAttribute("usuario");
                    if (!(id == null)) {
                        try (DAOFactory daoFactory = new DAOFactory()) {
                            daozao = daoFactory.getUsuarioDAO();

                            usuario = daozao.read(id);

                        } catch (SQLException | SecurityException ex) {
                            System.err.println(ex.getMessage());
                        }

                    }
                }
                if (usuario == null) {
                    RequestDispatcher dispatchers = request.getRequestDispatcher("/view/login.jsp");
                    dispatchers.forward(request, response);

                } else {
                    request.setAttribute("usuario", usuario);

                    try (DAOFactory daoFactory = new DAOFactory();) {
                        daozinho = daoFactory.getPostDAO();
                        List<Post> retorna = daozinho.busca(arrobas, hashtags, (Integer) session.getAttribute("usuario"));
                        List<Post> comarroba = new ArrayList();
                        for (Post p : retorna) {
                            p = daozinho.retornaPostcomTag(p, usuario, (Integer) session.getAttribute("usuario"));

                            comarroba.add(p);

                        }
                        request.setAttribute("busca", comarroba);
                        dispatcher = request.getRequestDispatcher("/view/post/busca.jsp");
                        dispatcher.forward(request, response);

                    } catch (SQLException ex) {
                        System.err.println(ex.getMessage());
                        response.sendRedirect(request.getContextPath() + "/");
                    }
                }

                break;
            case "/post/posthashtag":
                session = request.getSession(false);

                if (session != null) {
                    Integer id = (Integer) session.getAttribute("usuario");
                    if (!(id == null)) {
                        try (DAOFactory daoFactory = new DAOFactory()) {
                            daozao = daoFactory.getUsuarioDAO();

                            usuario = daozao.read(id);

                        } catch (SQLException | SecurityException ex) {
                            System.err.println(ex.getMessage());
                        }

                    }
                }
                if (usuario == null) {
                    RequestDispatcher dispatchers = request.getRequestDispatcher("/view/login.jsp");
                    dispatchers.forward(request, response);

                } else {
                    request.setAttribute("usuario", usuario);

                    try (DAOFactory daoFactory = new DAOFactory();) {
                        daozinho = daoFactory.getPostDAO();

                        List<Post> postsHashtag = daozinho.retornaPostsHashtag(request.getParameter("hashtag"));
                        List<Post> comarroba = new ArrayList();
                        for (Post p : postsHashtag) {
                            p = daozinho.retornaPostcomTag(p, usuario, (Integer) session.getAttribute("usuario"));

                            comarroba.add(p);

                        }

                        request.setAttribute("listaHashtag", comarroba);

                        dispatcher = request.getRequestDispatcher("/view/post/posthashtag.jsp");
                        dispatcher.forward(request, response);

                    } catch (SQLException ex) {
                        System.err.println(ex.getMessage());
                        response.sendRedirect(request.getContextPath() + "/");
                    }
                }

            case "/post/comentario":
                session = request.getSession(false);

                if (session != null) {
                    Integer id = (Integer) session.getAttribute("usuario");
                    if (!(id == null)) {
                        try (DAOFactory daoFactory = new DAOFactory()) {
                            daozao = daoFactory.getUsuarioDAO();

                            usuario = daozao.read(id);

                        } catch (SQLException | SecurityException ex) {
                            System.err.println(ex.getMessage());
                        }

                    }
                }
                if (usuario == null) {
                    RequestDispatcher dispatchers = request.getRequestDispatcher("/view/login.jsp");
                    dispatchers.forward(request, response);

                } else {
                    try (DAOFactory daoFactory = new DAOFactory();) {
                        daozinho = daoFactory.getPostDAO();

                        List<Post> listaComentario = daozinho.listaComentario(Integer.parseInt(request.getParameter("id_post")));
                        List<Post> comarroba = new ArrayList();
                        for (Post p : listaComentario) {

                            // System.out.println(p.getComentario());
                            p = daozinho.retornaComentariocomArroba(p, usuario, (Integer) session.getAttribute("usuario"));

                            comarroba.add(p);

                        }

                        request.setAttribute("listaComentario", comarroba);

                        dispatcher = request.getRequestDispatcher("/view/post/comentario.jsp");
                        dispatcher.forward(request, response);
                    } catch (SQLException ex) {
                        System.err.println(ex.getMessage());
                        response.sendRedirect(request.getContextPath() + "/");
                    }
                }
                break;

            case "/post/comentariomeus":
                session = request.getSession(false);

                if (session != null) {
                    Integer id = (Integer) session.getAttribute("usuario");
                    if (!(id == null)) {
                        try (DAOFactory daoFactory = new DAOFactory()) {
                            daozao = daoFactory.getUsuarioDAO();

                            usuario = daozao.read(id);

                        } catch (SQLException | SecurityException ex) {
                            System.err.println(ex.getMessage());
                        }

                    }
                }
                if (usuario == null) {
                    RequestDispatcher dispatchers = request.getRequestDispatcher("/view/login.jsp");
                    dispatchers.forward(request, response);

                } else {
                    try (DAOFactory daoFactory = new DAOFactory();) {
                        daozinho = daoFactory.getPostDAO();

                        List<Post> listaComentario = daozinho.listaComentariosMeusPosts(Integer.parseInt(request.getParameter("id_post")));
                        List<Post> comarroba = new ArrayList();
                        for (Post p : listaComentario) {
                            p = daozinho.retornaComentariocomArroba(p, usuario, (Integer) session.getAttribute("usuario"));

                            comarroba.add(p);

                        }

                        request.setAttribute("listaComentario", comarroba);
                        /*  for(Post i : listaComentario)
                         {
                         System.out.println(i);
                         }*/

                        dispatcher = request.getRequestDispatcher("/view/post/comentariomeus.jsp");
                        dispatcher.forward(request, response);
                    } catch (SQLException ex) {
                        System.err.println(ex.getMessage());
                        response.sendRedirect(request.getContextPath() + "/");
                    }
                }
                break;
            case "/post/like":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    daozinho = daoFactory.getPostDAO();
                    daozinho.like(Integer.parseInt(request.getParameter("id_usuario")), Integer.parseInt(request.getParameter("id_post")));

                    //request.setAttribute("teste", true);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendError(404);
                }
                response.sendRedirect(request.getContextPath() + "/");
                break;

            case "/post/unlike":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    daozinho = daoFactory.getPostDAO();
                    daozinho.removeLike(Integer.parseInt(request.getParameter("id_usuario")), Integer.parseInt(request.getParameter("id_post")));

                    //request.setAttribute("teste", true);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendError(404);
                }
                response.sendRedirect(request.getContextPath() + "/");
                break;

            case "/post/undislike":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    daozinho = daoFactory.getPostDAO();
                    daozinho.removeDislike(Integer.parseInt(request.getParameter("id_usuario")), Integer.parseInt(request.getParameter("id_post")));

                    //request.setAttribute("teste", true);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendError(404);
                }
                response.sendRedirect(request.getContextPath() + "/");
                break;

            case "/post/dislike":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    daozinho = daoFactory.getPostDAO();
                    daozinho.dislike(Integer.parseInt(request.getParameter("id_usuario")), Integer.parseInt(request.getParameter("id_post")));

                    //request.setAttribute("teste", true);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendError(404);
                }
                response.sendRedirect(request.getContextPath() + "/");
                break;

            case "/post/apagacoment":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    daozinho = daoFactory.getPostDAO();
                    daozinho.deletaComentario(Integer.parseInt(request.getParameter("id_comentario")));

                    // request.setAttribute("teste", false);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }

                response.sendRedirect(request.getContextPath() + "/");
                break;

            case "/post/top20":
                session = request.getSession(false);

                if (session != null) {
                    Integer id = (Integer) session.getAttribute("usuario");
                    if (!(id == null)) {
                        try (DAOFactory daoFactory = new DAOFactory()) {
                            daozao = daoFactory.getUsuarioDAO();

                            usuario = daozao.read(id);

                        } catch (SQLException | SecurityException ex) {
                            System.err.println(ex.getMessage());
                        }

                    }
                }
                if (usuario == null) {
                    RequestDispatcher dispatchers = request.getRequestDispatcher("/view/login.jsp");
                    dispatchers.forward(request, response);

                } else {
                    request.setAttribute("usuario", usuario);

                    try (DAOFactory daoFactory = new DAOFactory();) {
                        daozinho = daoFactory.getPostDAO();

                        // post.setId_usuario(usuario.getId());
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
                        Timestamp data_inicio;
                        Timestamp data_fim;
                        try {
                            data_inicio = new Timestamp(format.parse(request.getParameter("data_inicio")).getTime());
                            data_fim = new Timestamp(format.parse(request.getParameter("data_fim")).getTime());
                        } catch (ParseException ex) {
                            Logger.getLogger(PostController.class.getName()).log(Level.SEVERE, null, ex);
                            response.sendRedirect(request.getContextPath() + "/timeline");
                            break;
                        }
                        List<Post> postList = daozinho.top20postagens(data_inicio, data_fim);

                        List<Post> posts = new ArrayList();
                        for (Post p : postList) {
                            p = daozinho.retornaPostcomTag(p, usuario, (Integer) session.getAttribute("usuario"));

                            posts.add(p);

                        }
                        request.setAttribute("postList", posts);

                        dispatcher = request.getRequestDispatcher("/view/post/top20.jsp");
                        dispatcher.forward(request, response);
                    } catch (SQLException ex) {
                        System.err.println(ex.getMessage());
                        response.sendRedirect(request.getContextPath() + "/post");
                    }

                }
                break;

            case "/post/top3":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    daozinho = daoFactory.getPostDAO();
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat format2 = new SimpleDateFormat("hh:mm");
                    Calendar data_inicio;
                    Calendar data_fim;
                    Grafico grafico = new Grafico();
                    Calendar calendar = new GregorianCalendar();
                    Map<Integer, Dataset> maperino = new TreeMap<>();

                    try {
                        calendar.setTime(format.parse(request.getParameter("data")));
                    } catch (ParseException ex) {
                        Logger.getLogger(PostController.class.getName()).log(Level.SEVERE, null, ex);
                        response.sendRedirect(request.getContextPath() + "/timeline");
                        break;
                    }
                    for (int i = 0; i < 24; i++) {

                        data_inicio = (Calendar) calendar.clone();
                        data_fim = (Calendar) calendar.clone();

                        data_inicio.add(Calendar.HOUR_OF_DAY, i);
                        data_fim.add(Calendar.HOUR_OF_DAY, i + 1);
                        data_fim.add(Calendar.SECOND, -1);

                        Timestamp inicio = new Timestamp(data_inicio.getTimeInMillis());
                        Timestamp fim = new Timestamp(data_fim.getTimeInMillis());
                        grafico.labels.add(format2.format(data_inicio.getTime()));

                        List<Usuario> usuarioList = daozinho.top3usuario(inicio, fim);
                        for (Usuario s : usuarioList) {
                            Dataset dataset;
                            if (maperino.containsKey(s.getId())) {
                                dataset = maperino.get(s.getId());
                            } else {
                                dataset = new Dataset();
                                for (int j = 0; j < i; j++) {

                                    dataset.data.add(0);
                                }
                                maperino.put(s.getId(), dataset);
                                grafico.datasets.add(dataset);
                            }

                            dataset.label = s.getNome();

                            dataset.data.add(s.getPontuacao());

                        }

                        for (Dataset dataset : grafico.datasets) {
                            if (dataset.data.size() < i + 1) {
                                dataset.data.add(0);
                            }
                        }
                    }
                    int l = 0;
                    int x = 7;
                    for (Dataset dataset : grafico.datasets) {
                        dataset.setCor((l+x) * (360 / grafico.datasets.size() % 360), 100, 50);

                        l++;
                    }
                    request.setAttribute("grafico", gson.toJson(grafico));
                    request.setAttribute("datasets", grafico.datasets);
                    //  request.setAttribute("usuarioList", usuarioList);

                    dispatcher = request.getRequestDispatcher("/view/post/top3.jsp");
                    dispatcher.forward(request, response);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/post");
                }

                break;

            case "/post/topusu":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    daozinho = daoFactory.getPostDAO();
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
                    Timestamp data_inicio;
                    Timestamp data_fim;
                    try {
                        data_inicio = new Timestamp(format.parse(request.getParameter("data_inicio")).getTime());
                        data_fim = new Timestamp(format.parse(request.getParameter("data_fim")).getTime());
                    } catch (ParseException ex) {
                        Logger.getLogger(PostController.class.getName()).log(Level.SEVERE, null, ex);
                        response.sendRedirect(request.getContextPath() + "/post");
                        break;
                    }

                    List<Usuario> usuarioList = daozinho.top20usuario(data_inicio, data_fim);
                    request.setAttribute("usuarioList", usuarioList);

                    dispatcher = request.getRequestDispatcher("/view/post/topusu.jsp");
                    dispatcher.forward(request, response);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/post");
                }

                break;
            /* case "/post/influencia/":
             session = request.getSession(false);

             if (session != null) {
             Integer id = (Integer) session.getAttribute("usuario");
             if (!(id == null)) {
             try (DAOFactory daoFactory = new DAOFactory()) {
             daozao = daoFactory.getUsuarioDAO();

             usuario = daozao.read(id);

             } catch (SQLException | SecurityException ex) {
             System.err.println(ex.getMessage());
             }

             }
             }
             if (usuario == null) {
             RequestDispatcher dispatchers = request.getRequestDispatcher("/view/login.jsp");
             dispatchers.forward(request, response);

             } else {
             request.setAttribute("usuario", usuario);
             try (DAOFactory daoFactory = new DAOFactory();) {
             daozinho = daoFactory.getPostDAO();

             Integer retorno = daozinho.zona_influencia((Integer) session.getAttribute("usuario"));
             request.setAttribute("retorno", retorno);

             dispatcher = request.getRequestDispatcher("/view/post/influencia.jsp");
             dispatcher.forward(request, response);
             } catch (SQLException ex) {
             System.err.println(ex.getMessage());
             response.sendRedirect(request.getContextPath() + "/post");
             }
             }

             break;*/

            case "/post/similaridade":
                session = request.getSession(false);

                if (session != null) {
                    Integer id = (Integer) session.getAttribute("usuario");
                    if (!(id == null)) {
                        try (DAOFactory daoFactory = new DAOFactory()) {
                            daozao = daoFactory.getUsuarioDAO();

                            usuario = daozao.read(id);

                        } catch (SQLException | SecurityException ex) {
                            System.err.println(ex.getMessage());
                        }

                    }
                }
                if (usuario == null) {
                    RequestDispatcher dispatchers = request.getRequestDispatcher("/view/login.jsp");
                    dispatchers.forward(request, response);

                } else {
                    try (DAOFactory daoFactory = new DAOFactory();) {
                        daozinho = daoFactory.getPostDAO();

                        List<Usuario> usuarioList = daozinho.Similaridade((Integer) session.getAttribute("usuario"));
                        request.setAttribute("usuarioList", usuarioList);

                        dispatcher = request.getRequestDispatcher("/view/post/similaridade.jsp");
                        dispatcher.forward(request, response);
                    } catch (SQLException ex) {
                        System.err.println(ex.getMessage());
                        response.sendRedirect(request.getContextPath() + "/post");
                    }
                }

                break;

            case "/post":
                session = request.getSession(false);

                if (session != null) {
                    Integer id = (Integer) session.getAttribute("usuario");
                    if (!(id == null)) {
                        try (DAOFactory daoFactory = new DAOFactory()) {
                            daozao = daoFactory.getUsuarioDAO();

                            usuario = daozao.read(id);

                        } catch (SQLException | SecurityException ex) {
                            System.err.println(ex.getMessage());
                        }

                    }
                }
                if (usuario == null) {
                    RequestDispatcher dispatchers = request.getRequestDispatcher("/view/login.jsp");
                    dispatchers.forward(request, response);

                } else {
                    request.setAttribute("usuario", usuario);

                    try (DAOFactory daoFactory = new DAOFactory();) {
                        daozinho = daoFactory.getPostDAO();

                        // post.setId_usuario(usuario.getId());
                        List<Post> postList = daozinho.all();

                        List<Post> posts = new ArrayList();
                        for (Post p : postList) {
                            p = daozinho.retornaPostcomTag(p, usuario, (Integer) session.getAttribute("usuario"));

                            posts.add(p);

                        }
                        request.setAttribute("postList", posts);

                        dispatcher = request.getRequestDispatcher("/view/post/index.jsp");
                        dispatcher.forward(request, response);
                    } catch (SQLException ex) {
                        System.err.println(ex.getMessage());
                        response.sendRedirect(request.getContextPath() + "/post");
                    }

                }

        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAO dao;
        PostDAO daozinho;
        Post post = new Post();
        //Usuario usuario = new Usuario();

        switch (request.getServletPath()) {
            case "/post/create":

                try (DAOFactory daoFactory = new DAOFactory();) {
                    post.setId_usuario(Integer.parseInt(request.getParameter("id_usuario")));
                    String conteudo = request.getParameter("texto");
                    post.setTexto(conteudo);

                    dao = daozinho = daoFactory.getPostDAO();

                    dao.create(post);
                    String[] split = conteudo.split(" ");
                    Set<String> hashtags = new HashSet();
                    for (int i = 0; i < split.length; i++) {
                        if (split[i].startsWith("#")) {
                            String hashtag = split[i].substring(1);
                            hashtags.add(hashtag);

                        }
                    }
                    for (String hashtag : hashtags) {
                        daozinho.insereHashtag(post.getId_post(), hashtag);

                    }
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }

                response.sendRedirect(request.getContextPath() + "/post");
                break;
            case "/post/repost":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    daozinho = daoFactory.getPostDAO();

                    daozinho.repostar(Integer.parseInt(request.getParameter("id_post")), Integer.parseInt(request.getParameter("id_usuario")));

                    /* 
                     post.setId_original(Integer.parseInt(request.getParameter("id_post")));
                     post.setId_usuario(Integer.parseInt(request.getParameter("id_usuario")));
                     post.setTexto(request.getParameter("texto"));
                   
                     // post.setData(Date.valueOf(request.getParameter("data")));
                     daozinho.create(post); */
                    // request.setAttribute("teste", false);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }

                response.sendRedirect(request.getContextPath() + "/");
                break;

            case "/post/update":

                try (DAOFactory daoFactory = new DAOFactory();) {
                    post.setId_post(Integer.parseInt(request.getParameter("id_post")));
                    String conteudo = request.getParameter("texto");
                    post.setTexto(conteudo);

                    dao = daozinho = daoFactory.getPostDAO();

                    dao.update(post);
                    daozinho.deletaHashtag(post.getId_post());
                    String[] split = conteudo.split(" ");

                    Set<String> hashtags = new HashSet();
                    for (int i = 0; i < split.length; i++) {
                        if (split[i].startsWith("#")) {
                            String hashtag = split[i].substring(1);
                            hashtags.add(hashtag);

                        }
                    }
                    for (String hashtag : hashtags) {
                        daozinho.insereHashtag(post.getId_post(), hashtag);

                    }

                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }

                response.sendRedirect(request.getContextPath() + "/post");
                break;
            case "/post/comentario":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    daozinho = daoFactory.getPostDAO();
                    daozinho.criaComentario(Integer.parseInt(request.getParameter("id_post")), Integer.parseInt(request.getParameter("id_usuario")), request.getParameter("texto"));

                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendError(404);
                }
                response.sendRedirect(request.getContextPath() + "/");
                break;

            case "/post/delete":
                String[] posts = request.getParameterValues("delete");
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getPostDAO();
                    try {
                        daoFactory.beginTransaction();

                        for (int i = 0; i < posts.length; ++i) {
                            dao.delete(Integer.parseInt(posts[i]));
                        }

                        daoFactory.commitTransaction();
                        daoFactory.endTransaction();
                    } catch (SQLException ex) {
                        System.err.println(ex.getMessage());
                        daoFactory.rollbackTransaction();
                    }
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }

                response.sendRedirect(request.getContextPath() + "/post");

                break;

        }
    }
    static Gson gson = new GsonBuilder()
            .setPrettyPrinting()
            .setDateFormat("dd/MM/yyyy HH:mm")
            .create();

    static class Grafico {

        List<String> labels = new LinkedList<>();
        List<Dataset> datasets = new LinkedList<>();
    }

    public static class Dataset {

        public void setCor(int hue, int saturacao, int light) {
            fillColor = "hsla(" + hue + "," + saturacao + "%," + light + "%,0.2)";
            strokeColor = "hsla(" + hue + "," + saturacao + "%," + light + "%,1)";
            pointColor = "hsla(" + hue + "," + saturacao + "%," + light + "%,1)";
            pointHighlightStroke = "hsla(" + hue + "," + saturacao + "%," + light + "%,1)";

        }

        String label = "My First dataset";

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getStrokeColor() {
            return strokeColor;
        }

        public void setStrokeColor(String strokeColor) {
            this.strokeColor = strokeColor;
        }
        String fillColor;
        String strokeColor;
        String pointColor;
        String pointStrokeColor = "#fff";
        String pointHighlightFill = "#fff";
        String pointHighlightStroke;
        List<Integer> data = new LinkedList<>();//: [28, 48, 40, 19, 86, 27, 90]

    }
}
