package controller;

import dao.DAO;
import dao.DAOFactory;
import dao.PostDAO;
import dao.UsuarioDAO;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import model.Post;
import model.Usuario;

@WebServlet(urlPatterns = {"/usuario/create",
    "/usuario/read",
    "/usuario/update",
    "/usuario/delete",
    "/usuario/foto",
    "/usuario/seguir",
    "/usuario/naoseguir",
    "/usuario/buscausuario",
    "/usuario"})

@MultipartConfig
public class UsuarioController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAO dao;
        PostDAO daozinho;

        UsuarioDAO daousu;
        Usuario usuario = new Usuario();
        HttpSession session;
        RequestDispatcher dispatcher;
        request.setAttribute("autenticado", request.getSession().getAttribute("usuario"));
        switch (request.getServletPath()) {
            case "/usuario/create":
                dispatcher = request.getRequestDispatcher("/view/usuario/create.jsp");
                dispatcher.forward(request, response);

                break;
            case "/usuario/read":
                session = request.getSession(false);

                if (session != null) {
                    Integer id = (Integer) session.getAttribute("usuario");
                    if (!(id == null)) {
                        try (DAOFactory daoFactory = new DAOFactory()) {
                            daousu = daoFactory.getUsuarioDAO();

                            usuario = daousu.read(id);

                        } catch (SQLException | SecurityException ex) {
                            System.err.println(ex.getMessage());
                        }

                    }
                }
                if (usuario == null) {
                    RequestDispatcher dispatchers = request.getRequestDispatcher("/view/login.jsp");
                    dispatchers.forward(request, response);

                } else {

                    try (DAOFactory daoFactory = new DAOFactory();) {
                        daousu = daoFactory.getUsuarioDAO();
                        daozinho = daoFactory.getPostDAO();

                        Integer retorno = daozinho.zona_influencia(Integer.parseInt(request.getParameter("id")));
                        request.setAttribute("retorno", retorno);
                        usuario = (Usuario) daousu.read(Integer.parseInt(request.getParameter("id")));
                        request.setAttribute("usuario", usuario);

                        daozinho = daoFactory.getPostDAO();
                        List<Post> posterinos = new ArrayList<Post>();
                        posterinos = daozinho.getTodosPosts(usuario.getId());
                        List<Post> comarroba = new ArrayList();
                        for (Post p : posterinos) {
                            p = daozinho.retornaPostcomTag(p, usuario, (Integer) session.getAttribute("usuario"));

                            comarroba.add(p);

                        }

                        request.setAttribute("posts", comarroba);

                        boolean teste = daousu.verificaSeguidor(Integer.parseInt(request.getParameter("id")), Integer.parseInt(request.getParameter("id_usuario2")));

                        request.setAttribute("teste", teste);

                        dispatcher = request.getRequestDispatcher("/view" + request.getServletPath() + ".jsp");
                        dispatcher.forward(request, response);
                    } catch (SQLException ex) {
                        System.err.println(ex.getMessage());
                        response.sendRedirect(request.getContextPath() + "/usuario");
                    }
                }

                break;

            case "/usuario/buscausuario":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    daousu = daoFactory.getUsuarioDAO();
                    usuario.setNome(request.getParameter("nome"));
                    List<Usuario> retorna = daousu.busca(usuario);
                  /*  for(Usuario u : retorna)
                    {
                        System.out.println(u.getNome());
                    
                    }*/
                    request.setAttribute("busca", retorna);
                    dispatcher = request.getRequestDispatcher("/view/usuario/buscausuario.jsp");
                    dispatcher.forward(request, response);

                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/");
                }

                break;
            case "/usuario/update":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getUsuarioDAO();

                    usuario = (Usuario) dao.read(Integer.parseInt(request.getParameter("id")));
                    request.setAttribute("usuario", usuario);

                    dispatcher = request.getRequestDispatcher("/view" + request.getServletPath() + ".jsp");
                    dispatcher.forward(request, response);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/usuario");
                }

                break;
            case "/usuario/delete":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getUsuarioDAO();

                    dao.delete(Integer.parseInt(request.getParameter("id")));
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }

                response.sendRedirect(request.getContextPath() + "/");

                break;
            case "/usuario/seguir":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    daousu = daoFactory.getUsuarioDAO();
                    daousu.seguir(Integer.parseInt(request.getParameter("id_usuario1")), Integer.parseInt(request.getParameter("id_usuario2")));

                    //request.setAttribute("teste", true);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendError(404);
                }
                response.sendRedirect(request.getContextPath() + "/usuario");
                break;

            case "/usuario/naoseguir":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    daousu = daoFactory.getUsuarioDAO();
                    daousu.deletaSeguir(Integer.parseInt(request.getParameter("id_usuario1")), Integer.parseInt(request.getParameter("id_usuario2")));

                    // request.setAttribute("teste", false);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }

                response.sendRedirect(request.getContextPath() + "/usuario");
                break;

            case "/usuario/foto":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getUsuarioDAO();

                    usuario = (Usuario) dao.read(Integer.parseInt(request.getParameter("id")));
                    InputStream salva = usuario.getFoto();
                    if (salva == null) {
                        response.sendError(404);
                        break;
                    }
                    response.setContentType(usuario.getTipofoto());
                    try (ServletOutputStream output = response.getOutputStream()) {
                        byte[] buffer = new byte[2048];
                        while (true) {

                            int tamanho = salva.read(buffer);
                            if (tamanho == -1) {
                                return;
                            }
                            output.write(buffer);
                        }

                    }
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendError(404);
                }

                break;
            case "/usuario":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getUsuarioDAO();

                    List<Usuario> usuarioList = dao.all();
                    request.setAttribute("usuarioList", usuarioList);

                    dispatcher = request.getRequestDispatcher("/view/usuario/index.jsp");
                    dispatcher.forward(request, response);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/usuario");
                }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAO dao;
        Usuario usuario = new Usuario();

        switch (request.getServletPath()) {
            case "/usuario/create":
                usuario.setLogin(request.getParameter("login"));
                usuario.setSenha(request.getParameter("senha"));
                usuario.setNome(request.getParameter("nome"));
                usuario.setNascimento(Date.valueOf(request.getParameter("nascimento")));
                usuario.setDescricao(request.getParameter("descricao"));

                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getUsuarioDAO();

                    dao.create(usuario);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }

                

                break;
            case "/usuario/update":
                usuario.setId(Integer.parseInt(request.getParameter("id")));
                usuario.setLogin(request.getParameter("login"));
                usuario.setSenha(request.getParameter("senha"));
                usuario.setNome(request.getParameter("nome"));
                usuario.setNascimento(Date.valueOf(request.getParameter("nascimento")));
                usuario.setDescricao(request.getParameter("descricao"));

                Part foto = request.getPart("foto");
                if (foto != null && foto.getSize() > 0) {
                    usuario.setTipofoto(foto.getContentType());
                    usuario.setFoto(foto.getInputStream());
                    usuario.setTamanho(foto.getSize());
                }
                String senha = request.getParameter("senha");
                if (senha == null || senha.isEmpty()) {
                    usuario.setSenha(null);
                } else {
                    usuario.setSenha(senha);
                }
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getUsuarioDAO();

                    dao.update(usuario);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }

                response.sendRedirect(request.getContextPath() + "/usuario");

                break;
            case "/usuario/delete":
                String[] usuarios = request.getParameterValues("delete");

                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getUsuarioDAO();

                    try {
                        daoFactory.beginTransaction();

                        for (int i = 0; i < usuarios.length; ++i) {
                            dao.delete(Integer.parseInt(usuarios[i]));
                        }

                        daoFactory.commitTransaction();
                        daoFactory.endTransaction();
                    } catch (SQLException ex) {
                        System.err.println(ex.getMessage());
                        daoFactory.rollbackTransaction();
                    }
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }

                response.sendRedirect(request.getContextPath() + "/usuario");

                break;
        }
    }
}
