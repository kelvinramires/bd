/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dao.DAOFactory;
import dao.PostDAO;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Comentario;
import model.Dislikes;
import model.Likes;
import model.Post;
import model.Repost;
import model.Usuario;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 *
 * @author Kelvin
 */
@WebServlet(name = "WebserverController", urlPatterns = {"/importar", "/exportar"})
public class WebserverviceController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch (request.getServletPath()) {
            case "/importar":
                importarGET(request, response);
                break;
            case "/exportar":
                Pacote pacote = new Pacote();

                try (DAOFactory factory = new DAOFactory()) {
                    List<Post> postsDB = factory.getPostDAO().allimportado();
                    List<Repost> repostsDB = factory.getPostDAO().allrepost();
                    List<Usuario> usuariosDB = factory.getUsuarioDAO().allimportado();
                    List<Comentario> comentariosDB = factory.getPostDAO().allComentario();
                    List<Likes> likesDB = factory.getPostDAO().allLikes();
                    List<Dislikes> dislikesDB = factory.getPostDAO().allDislikes();

                    for (Post postDB : postsDB) {
                        PostJSON postJSON = new PostJSON();
                        postJSON.id = postDB.getId_post();
                        postJSON.id_usuario = postDB.getId_usuario();
                        postJSON.titulo = postDB.getTitulo();//"izaias";
                        postJSON.conteudo = postDB.getTexto();
                        postJSON.data = postDB.getData();//new java.util.Date();
                        pacote.posts.add(postJSON);
                    }

                    for (Repost repostDB : repostsDB) {
                        RepostJSON repostJSON = new RepostJSON();
                        repostJSON.id = repostDB.getId();
                        repostJSON.id_post = repostDB.getId_post();
                        repostJSON.id_usuario = repostDB.getId_usuario();

                        repostJSON.data = repostDB.getData();//new java.util.Date();
                        pacote.reposts.add(repostJSON);

                    }

                    for (Usuario usuarioDB : usuariosDB) {
                        UsuarioJSON usuarioJSON = new UsuarioJSON();
                        usuarioJSON.id = usuarioDB.getId();
                        usuarioJSON.login = usuarioDB.getLogin();
                        usuarioJSON.nome = usuarioDB.getNome();
                        usuarioJSON.nascimento = usuarioDB.getNascimento();//new java.util.Date();
                        usuarioJSON.descricao = usuarioDB.getDescricao();
                        pacote.usuarios.add(usuarioJSON);
                    }

                    for (Comentario comentarioDB : comentariosDB) {
                        ComentarioJSON comentarioJSON = new ComentarioJSON();
                        comentarioJSON.id = comentarioDB.getId();
                        comentarioJSON.id_post = comentarioDB.getId_post();
                        comentarioJSON.id_usuario = comentarioDB.getId_usuario();
                        comentarioJSON.data = comentarioDB.getData();//new java.util.Date();
                        comentarioJSON.conteudo = comentarioDB.getConteudo();
                        pacote.comentarios.add(comentarioJSON);
                    }

                    for (Likes likeDB : likesDB) {
                        LikeJSON likeJSON = new LikeJSON();

                        likeJSON.id_post = likeDB.getId_post();
                        likeJSON.id_usuario = likeDB.getId_usuario();
                        likeJSON.data = likeDB.getData();//new java.util.Date();

                        pacote.likes.add(likeJSON);
                    }

                    for (Dislikes dislikeDB : dislikesDB) {
                        DislikeJSON dislikeJSON = new DislikeJSON();

                        dislikeJSON.id_post = dislikeDB.getId_post();
                        dislikeJSON.id_usuario = dislikeDB.getId_usuario();
                        dislikeJSON.data = dislikeDB.getData();//new java.util.Date();

                        pacote.dislikes.add(dislikeJSON);
                    }

                } catch (SQLException ex) {
                    throw new ServletException(ex);
                }
                Gson gson = new GsonBuilder()
                        .setPrettyPrinting()
                        .setDateFormat("dd/MM/yyyy HH:mm")
                        .create();

                response.setCharacterEncoding("utf-8");
                response.setContentType("application/json");

                try (PrintWriter out = response.getWriter()) {
                    gson.toJson(pacote, out);

                }
                break;
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch (request.getServletPath()) {
            case "/importar":
                importarPOST(request, response);
                response.sendRedirect(request.getContextPath() + "/");
                break;
        }
    }

    static Gson gson = new GsonBuilder()
            .setPrettyPrinting()
            .setDateFormat("dd/MM/yyyy HH:mm")
            .create();

    private void importarPOST(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String url = request.getParameter("texto");
        Pacote pacote = pegarPacoteDaURL(url);

        Map<Integer, Integer> mapaIdUsuarios = new TreeMap<>();
        Map<Integer, Integer> mapaIdPosts = new TreeMap<>();
        try (DAOFactory factory = new DAOFactory()) {
            
            factory.getPostDAO().deletarTudo(pacote.id_servidor);
            
            
            factory.beginTransaction();

            try {

                for (UsuarioJSON usuarioJSON : pacote.usuarios) {

                    Usuario usuarioBD = new Usuario();
                    usuarioBD.setNome(usuarioJSON.nome);
                    usuarioBD.setLogin(usuarioJSON.login + "_" + pacote.id_servidor);
                    usuarioBD.setNascimento(new java.sql.Date(usuarioJSON.nascimento.getTime()));
                    usuarioBD.setDescricao(usuarioJSON.descricao);
                    usuarioBD.setServidor(pacote.id_servidor);
                    factory.getUsuarioDAO().createImportado(usuarioBD);

                    int idLocal = usuarioBD.getId();
                    int idRemoto = usuarioJSON.id;

                    mapaIdUsuarios.put(idRemoto, idLocal);
                }

                for (PostJSON postJSON : pacote.posts) {
                    Post postBD = new Post();
                    //postBD.setId_post(postJSON.id);

                    postBD.setTexto(postJSON.conteudo);
                    postBD.setTitulo(postJSON.titulo);
                    postBD.setData(new java.sql.Timestamp(postJSON.data.getTime()));
                    postBD.setServidor(pacote.id_servidor);

                    int idRemoto = postJSON.id_usuario;
                    int idLocal = mapaIdUsuarios.get(idRemoto);

                    postBD.setId_usuario(idLocal);
                    factory.getPostDAO().createImportado(postBD);
                    mapaIdPosts.put(postJSON.id, postBD.getId_post());
                }

                for (RepostJSON repostJSON : pacote.reposts) {
                    Repost repostBD = new Repost();

                    //repostBD.setData(new java.sql.Timestamp(repostJSON.data.getTime()));
                    repostBD.setServidor(pacote.id_servidor);

                    int idUsuarioRemoto = repostJSON.id_usuario;
                    int idUsuarioLocal = mapaIdUsuarios.get(idUsuarioRemoto);

                    repostBD.setId_usuario(idUsuarioLocal);

                    int idPostRemoto = repostJSON.id_post;
                    int idPostLocal = mapaIdPosts.get(idPostRemoto);

                    repostBD.setId_post(idPostLocal);

                    factory.getPostDAO().repostar(repostBD);
                }

                for (LikeJSON likeJSON : pacote.likes) {

                    Likes likeBD = new Likes();

                    // likeBD.setData(new java.sql.Timestamp(likeJSON.data.getTime()));
                    likeBD.setServidor(pacote.id_servidor);

                    int idUsuarioRemoto = likeJSON.id_usuario;
                    int idUsuarioLocal = mapaIdUsuarios.get(idUsuarioRemoto);

                    likeBD.setId_usuario(idUsuarioLocal);

                    int idPostRemoto = likeJSON.id_post;
                    System.out.println(idPostRemoto);
                    int idPostLocal = mapaIdPosts.get(idPostRemoto);

                    likeBD.setId_post(idPostLocal);

                    factory.getPostDAO().like(likeBD);
                }
                for (DislikeJSON dislikeJSON : pacote.dislikes) {

                    Dislikes dislikeBD = new Dislikes();

                    // likeBD.setData(new java.sql.Timestamp(likeJSON.data.getTime()));
                    dislikeBD.setServidor(pacote.id_servidor);

                    int idUsuarioRemoto = dislikeJSON.id_usuario;
                    int idUsuarioLocal = mapaIdUsuarios.get(idUsuarioRemoto);

                    dislikeBD.setId_usuario(idUsuarioLocal);

                    int idPostRemoto = dislikeJSON.id_post;
                    int idPostLocal = mapaIdPosts.get(idPostRemoto);

                    dislikeBD.setId_post(idPostLocal);

                    factory.getPostDAO().dislike(dislikeBD);
                }

                for (ComentarioJSON comentarioJSON : pacote.comentarios) {

                    Comentario comentarioBD = new Comentario();

                    // likeBD.setData(new java.sql.Timestamp(likeJSON.data.getTime()));
                    comentarioBD.setServidor(pacote.id_servidor);
                    comentarioBD.setConteudo(comentarioJSON.conteudo);

                    int idUsuarioRemoto = comentarioJSON.id_usuario;
                    int idUsuarioLocal = mapaIdUsuarios.get(idUsuarioRemoto);

                    comentarioBD.setId_usuario(idUsuarioLocal);

                    int idPostRemoto = comentarioJSON.id_post;
                    int idPostLocal = mapaIdPosts.get(idPostRemoto);

                    comentarioBD.setId_post(idPostLocal);

                    factory.getPostDAO().criaComentario(comentarioBD);
                }
                factory.commitTransaction();
            } catch (Exception ex) {
                factory.rollbackTransaction();
                throw ex;
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        }
    }

    private Pacote pegarPacoteDaURL(String url) throws IOException {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            try (CloseableHttpResponse jsonResponse = httpClient.execute(new HttpGet(url)); InputStreamReader jsonContent = new InputStreamReader(jsonResponse.getEntity().getContent(), "utf-8")) {

                return gson.fromJson(jsonContent, Pacote.class);
            }
        }
    }

    private void importarGET(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/view/importar.jsp");
        try {
            dispatcher.forward(request, response);
        } catch (IOException ex) {
            Logger.getLogger(WebserverviceController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    static class Pacote {

        int id_servidor = 20;
        List<PostJSON> posts;
        List<RepostJSON> reposts;
        List<LikeJSON> likes;
        List<DislikeJSON> dislikes;
        List<ComentarioJSON> comentarios;
        List<UsuarioJSON> usuarios;

        public Pacote() {
            this.posts = new LinkedList<>();
            this.reposts = new LinkedList<>();
            this.likes = new LinkedList<>();
            this.dislikes = new LinkedList<>();
            this.comentarios = new LinkedList<>();
            this.usuarios = new LinkedList<>();

        }
    }

    static class PostJSON {

        int id;
        int id_usuario;
        String titulo;
        String conteudo;
        java.util.Date data;

    }

    static class RepostJSON {

        int id;
        int id_usuario;
        int id_post;
        java.util.Date data;

    }

    static class LikeJSON {

        int id_post;
        int id_usuario;

        java.util.Date data;

    }

    static class DislikeJSON {

        int id_post;
        int id_usuario;

        java.util.Date data;

    }

    static class ComentarioJSON {

        int id;
        int id_post;
        int id_usuario;
        String conteudo;
        java.util.Date data;

    }

    static class UsuarioJSON {

        int id;
        String nome;
        String login;
        java.util.Date nascimento;
        String descricao;

    }

}
