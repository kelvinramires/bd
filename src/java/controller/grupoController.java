package controller;

import dao.DAO;
import dao.DAOFactory;
import dao.GrupoDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Grupo;
import model.Usuario;

/**
 *
 * @author Kelvin
 */
@WebServlet(urlPatterns = {"/grupo/create",
    "/grupo/read",
    "/grupo/update",
    "/grupo/delete",
    "/grupo/adicionar",
    "/grupo/remover",
    "/grupo"})

public class grupoController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("autenticado", request.getSession().getAttribute("usuario"));

        DAO daopai;
        GrupoDAO daozinho;
        RequestDispatcher dispatcher;

        switch (request.getServletPath()) {
            case "/grupo/create":
                dispatcher = request.getRequestDispatcher("/view/grupo/create.jsp");
                dispatcher.forward(request, response);
                break;

            case "/grupo/adicionar":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    daopai = daoFactory.getUsuarioDAO();

                    List<Usuario> usuarioList = daopai.all();
                    request.setAttribute("usuarioList", usuarioList);
                    request.setAttribute("id_grupo", Integer.parseInt(request.getParameter("id_grupo")));
                    dispatcher = request.getRequestDispatcher("/view/grupo/adicionar.jsp");
                    dispatcher.forward(request, response);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/grupo");
                }
                break;
            case "/grupo/remover":

                try (DAOFactory daoFactory = new DAOFactory();) {

                    daozinho = daoFactory.getGrupoDAO();
                    List<Usuario> UsuariosCadastradosList = daozinho.listar((Integer.parseInt(request.getParameter("id_grupo"))));

                    request.setAttribute("UsuariosCadastradosList", UsuariosCadastradosList);
                    request.setAttribute("id_grupo", Integer.parseInt(request.getParameter("id_grupo")));
                    dispatcher = request.getRequestDispatcher("/view/grupo/remover.jsp");
                    dispatcher.forward(request, response);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/grupo");
                }
                break;
            case "/grupo/read":
            case "/grupo/update":
                try (DAOFactory daoFactorys = new DAOFactory();) {

                    daozinho = daoFactorys.getGrupoDAO();
                    List<Usuario> UsuariosCadastradosList = daozinho.listar((Integer.parseInt(request.getParameter("id_grupo"))));

                    request.setAttribute("UsuariosCadastradosList", UsuariosCadastradosList);
                    request.setAttribute("id_grupo", Integer.parseInt(request.getParameter("id_grupo")));
                    try (DAOFactory daoFactory = new DAOFactory();) {
                        daopai = daoFactory.getGrupoDAO();

                        Grupo grupo = (Grupo) daopai.read(Integer.parseInt(request.getParameter("id_grupo")));
                        request.setAttribute("grupo", grupo);

                        dispatcher = request.getRequestDispatcher("/view" + request.getServletPath() + ".jsp");
                        dispatcher.forward(request, response);
                    } catch (SQLException ex) {
                        System.err.println(ex.getMessage());
                        response.sendRedirect(request.getContextPath() + "/grupo");
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(grupoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;

            case "/grupo/delete":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    daopai = daoFactory.getGrupoDAO();

                    daopai.delete(Integer.parseInt(request.getParameter("id_grupo")));
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }

                response.sendRedirect(request.getContextPath() + "/grupo");
                break;

            case "/grupo":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    daopai = daoFactory.getGrupoDAO();

                    List<Grupo> grupoList = daopai.all();
                    /* for(Grupo i : grupoList)
                     {
                     System.out.println(i.getId_grupo());
                     } */
                    request.setAttribute("grupoList", grupoList);

                    dispatcher = request.getRequestDispatcher("/view/grupo/index.jsp");
                    dispatcher.forward(request, response);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/grupo");
                }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        GrupoDAO dao;
        Grupo grupo = new Grupo();
        switch (request.getServletPath()) {
            case "/grupo/create":

                grupo.setId_usuario(Integer.parseInt(request.getParameter("id_usuario")));
                grupo.setNome(request.getParameter("nome"));
                grupo.setDescricao(request.getParameter("descricao"));
                grupo.setTag(request.getParameter("tag"));

                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getGrupoDAO();

                    dao.create(grupo);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }

                response.sendRedirect(request.getContextPath() + "/grupo");
                break;

            case "/grupo/update":

                grupo.setId_grupo(Integer.parseInt(request.getParameter("id_grupo")));
                grupo.setNome(request.getParameter("nome"));
                grupo.setDescricao(request.getParameter("descricao"));

                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getGrupoDAO();

                    dao.update(grupo);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }

                response.sendRedirect(request.getContextPath() + "/grupo");
                break;
            case "/grupo/adicionar":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getGrupoDAO();

                    dao.adicionar(Integer.parseInt(request.getParameter("id_grupo")), Integer.parseInt(request.getParameter("id_usuario")));
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }
                response.sendRedirect(request.getContextPath() + "/grupo");
                break;

            case "/grupo/remover":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getGrupoDAO();

                    dao.excluir(Integer.parseInt(request.getParameter("id_grupo")), Integer.parseInt(request.getParameter("id_usuario")));
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }
                response.sendRedirect(request.getContextPath() + "/grupo");
                break;

            case "/grupo/delete":
                String[] grupos = request.getParameterValues("delete");
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getGrupoDAO();
                    try {
                        daoFactory.beginTransaction();

                        for (int i = 0; i < grupos.length; ++i) {
                            dao.delete(Integer.parseInt(grupos[i]));
                        }

                        daoFactory.commitTransaction();
                        daoFactory.endTransaction();
                    } catch (SQLException ex) {
                        System.err.println(ex.getMessage());
                        daoFactory.rollbackTransaction();
                    }
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }

                response.sendRedirect(request.getContextPath() + "/grupo");
                break;

        }
    }
}
