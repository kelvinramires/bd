/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.sql.Timestamp;

/**
 *
 * @author Kelvin
 */
public class Dislikes {
        public Integer getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Integer id_usuario) {
        this.id_usuario = id_usuario;
    }

    public Integer getId_post() {
        return id_post;
    }

    public void setId_post(Integer id_post) {
        this.id_post = id_post;
    }

    public Timestamp getData() {
        return data;
    }

    public void setData(Timestamp data) {
        this.data = data;
    }
     private Integer id_usuario;
    private Integer id_post;
    private Timestamp data;
    
     private Integer servidor;

    public Integer getServidor() {
        return servidor;
    }

    public void setServidor(Integer servidor) {
        this.servidor = servidor;
    }
}
