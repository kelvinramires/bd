
package model;

import java.sql.Timestamp;


public class Repost {

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_post() {
        return id_post;
    }

    public void setId_post(Integer id_post) {
        this.id_post = id_post;
    }

    public Integer getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Integer id_usuario) {
        this.id_usuario = id_usuario;
    }

    public Timestamp getData() {
        return data;
    }

    public void setData(Timestamp data) {
        this.data = data;
    }
    
    private Integer id;
    private Integer id_post;
    private Integer id_usuario;
    private Timestamp data;
    private Integer servidor;

    public Integer getServidor() {
        return servidor;
    }

    public void setServidor(Integer servidor) {
        this.servidor = servidor;
    }
}
