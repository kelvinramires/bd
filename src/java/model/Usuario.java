package model;

import java.io.InputStream;
import java.sql.Date;

public class Usuario {

    private Integer id;
    private String login;
    private String senha;
    private String nome;
    private Date nascimento;
    private String descricao;
    private String tipofoto;
    private InputStream foto;
    private long tamanho;
    private Integer pontuacao;
    private Float influencia;
    private Integer servidor;

    public Integer getServidor() {
        return servidor;
    }

    public void setServidor(Integer servidor) {
        this.servidor = servidor;
    }

    public Float getInfluencia() {
        return influencia;
    }

    public void setInfluencia(Float influencia) {
        this.influencia = influencia;
    }

    public int getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }
    

    public long getTamanho() {
        return tamanho;
    }

    public void setTamanho(long tamanho) {
        this.tamanho = tamanho;
    }

    public String getTipofoto() {
        return tipofoto;
    }

    public void setTipofoto(String tipofoto) {
        this.tipofoto = tipofoto;
    }

    public InputStream getFoto() {
        return foto;
    }

    public void setFoto(InputStream foto) {
        this.foto = foto;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getNascimento() {
        return nascimento;
    }

    public void setNascimento(Date nascimento) {
        this.nascimento = nascimento;
    }
}