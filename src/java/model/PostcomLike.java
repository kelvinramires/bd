/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author Kelvin
 */
public class PostcomLike {

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Boolean getLike() {
        return like;
    }

    public void setLike(Boolean like) {
        this.like = like;
    }

    public Boolean getDislike() {
        return dislike;
    }

    public void setDislike(Boolean dislike) {
        this.dislike = dislike;
    }
    private Post post;
    private Boolean like;
    private Boolean dislike;

    public Integer getQtd_Like() {
        return qtd_Like;
    }

    public void setQtd_Like(Integer qtd_Like) {
        this.qtd_Like = qtd_Like;
    }

    public Integer getQtd_Dislike() {
        return qtd_Dislike;
    }

    public void setQtd_Dislike(Integer qtd_Dislike) {
        this.qtd_Dislike = qtd_Dislike;
    }
    private Integer qtd_Like;
    private Integer qtd_Dislike;
    
    
}
