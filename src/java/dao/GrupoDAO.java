package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Grupo;
import model.Usuario;

public class GrupoDAO extends DAO<Grupo> {

    private static final String createQuery = "INSERT INTO grupo(id_usuario, nome, descricao , tag) VALUES(?,?,?,?);";
    private static final String readQuery = "SELECT * FROM  grupo WHERE id_grupo = (?);";
    private static final String updateQuery = "UPDATE grupo SET nome = ?, descricao = ? WHERE id_grupo = (?);";
    private static final String deleteQuery = "DELETE FROM grupo WHERE id_grupo = (?);";
    private static final String allQuery = "SELECT * FROM grupo;";
    private static final String adicionarQuery = "INSERT INTO membros(id_grupo, id_usuario) VALUES (?,?);";
    private static final String excluirQuery = "DELETE FROM membros WHERE id_usuario = (?) AND id_grupo = (?);";
    private static final String listarQuery = "SELECT id, nome FROM usuario JOIN membros ON id_usuario = id WHERE id_grupo = (?);";

    public GrupoDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(Grupo grupo) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(createQuery);) {

            statement.setInt(1, grupo.getId_usuario());
            statement.setString(2, grupo.getNome());
            statement.setString(3, grupo.getDescricao());
            statement.setString(4, grupo.getTag());
            statement.executeUpdate();

        } catch (SQLException ex) {
            throw ex;
        }
    }

    @Override
    public Grupo read(Integer id_grupo) throws SQLException {
        Grupo grupo = new Grupo();
        try (PreparedStatement statement = connection.prepareStatement(readQuery);) {
            statement.setInt(1, id_grupo);
            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {

                    grupo.setId_grupo(result.getInt("id_grupo"));
                    grupo.setId_usuario(result.getInt("id_usuario"));
                    grupo.setNome(result.getString("nome"));
                    grupo.setDescricao(result.getString("descricao"));
                   

                } else {
                    throw new SQLException("Falha ao visualizar: Post não encontrado.");
                }
            }
        }
        return grupo;
    }

    @Override
    public void update(Grupo grupo) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(updateQuery);) {
            statement.setString(1, grupo.getNome());
            statement.setString(2, grupo.getDescricao());
            statement.setInt(3, grupo.getId_grupo());

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Falha ao editar: Post não encontrado.");
            }

        }
    }

    @Override
    public void delete(Integer id_grupo) throws SQLException {

        try (PreparedStatement statement = connection.prepareStatement(deleteQuery);) {
            statement.setInt(1, id_grupo);

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Falha ao excluir: usuário não encontrado.");
            }
        }
    }

    @Override
    public List<Grupo> all() throws SQLException {
        List<Grupo> grupoList = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(allQuery);
                ResultSet result = statement.executeQuery();) {
            while (result.next()) {
                Grupo grupo = new Grupo();

                grupo.setId_grupo(result.getInt("id_grupo"));
                grupo.setId_usuario(result.getInt("id_usuario"));
                grupo.setNome(result.getString("nome"));
                grupo.setDescricao(result.getString("descricao"));

                grupoList.add(grupo);
            }
        } catch (SQLException ex) {
            throw ex;
        }

        return grupoList;
    }

    public void adicionar(Integer id_grupo, Integer id_usuario) throws SQLException {

        try (PreparedStatement statement = connection.prepareStatement(adicionarQuery);) {

            statement.setInt(1, id_grupo);
            statement.setInt(2, id_usuario);

            statement.executeUpdate();
        } catch (SQLException ex) {
            throw ex;
        }
    }

    public void excluir(Integer id_grupo, Integer id_usuario) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(excluirQuery);) {
            statement.setInt(1, id_usuario);
            statement.setInt(2, id_grupo);

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Falha ao excluir: usuário não encontrado.");
            }
        }
    }

    public List<Usuario> listar(Integer id_grupo) throws SQLException {
        List<Usuario> UsuariosCadastradosList = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(listarQuery);) {
            statement.setInt(1, id_grupo);
            try (ResultSet result = statement.executeQuery();){
                {
                    while (result.next()) {
                        Usuario usuario = new Usuario();

                        usuario.setId(result.getInt("id"));
                        usuario.setNome(result.getString("nome"));
                        UsuariosCadastradosList.add(usuario);
                    }
                }
            } catch (SQLException ex) {
                throw ex;
            }

            return UsuariosCadastradosList;

        }
    }
}
