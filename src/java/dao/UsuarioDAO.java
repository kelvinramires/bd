package dao;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Usuario;

public class UsuarioDAO extends DAO<Usuario> {

    private static final String createQuery = "INSERT INTO usuario(login, senha, nome, nascimento,descricao) VALUES(?, md5(?), ?, ?,?) RETURNING ID;";

    private static final String createImportadoQuery = "INSERT INTO usuario(login,senha,nome, nascimento,descricao, servidor) VALUES(?,' ',?, ?,?,?) RETURNING ID;";
    private static final String readQuery = "SELECT * FROM usuario WHERE id = ?;";

    //updates
    private static final String updateWithFotoPasswordQuery = "UPDATE usuario SET login = ?, nome= ?, nascimento =?, descricao =?, tipofoto = ?, foto=? , senha = md5(?) WHERE id = ?";
    private static final String updateQuery = "UPDATE usuario SET login = ?, nome = ?, nascimento = ?, descricao =? WHERE id = ?;";
    private static final String updateWithFoto = "UPDATE usuario SET login = ?, nome= ?, nascimento =?, descricao =?, tipofoto = ?, foto=? WHERE id = ?";
    private static final String upadateWithPassowrd = "UPDATE usuario SET login = ?, nome = ?, nascimento = ?, descricao =?, senha = md5(?) WHERE id = ?;";

    private static final String deleteQuery = "DELETE FROM usuario WHERE id = ?;";
    private static final String allQuery = "SELECT * FROM usuario;";
    private static final String allMinhasQuery = "SELECT * FROM usuario WHERE servidor is NULL;";
    private static final String authenticateQuery = "SELECT id, senha, nome, nascimento FROM usuario WHERE login = ?;";

    //seguidores querys
    private static final String seguidoresQuery = "INSERT INTO seguidores (id_usuario1, id_usuario2) VALUES (?,?);";
    private static final String verificaSeguidor = "SELECT * FROM SEGUIDORES WHERE id_usuario1 = (?) AND id_usuario2 = (?);";
    private static final String delataseguidoresQuery = "DELETE FROM seguidores WHERE id_usuario1 =(?) AND id_usuario2 =(?);";

    //busca querys
    private static final String buscaQuery = "SELECT * FROM usuario WHERE nome like ?;";

    public UsuarioDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(Usuario usuario) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(createQuery);) {
            statement.setString(1, usuario.getLogin());
            statement.setString(2, usuario.getSenha());
            statement.setString(3, usuario.getNome());
            statement.setDate(4, usuario.getNascimento());
            statement.setString(5, usuario.getDescricao());
           // statement.executeUpdate();

            try (ResultSet result = statement.executeQuery()) {
                usuario.setId(result.getInt(1));

            }
        } catch (SQLException ex) {
            throw ex;
        }
    }

    public void createImportado(Usuario usuario) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(createImportadoQuery);) {
            statement.setString(1, usuario.getLogin());
          
            statement.setString(2, usuario.getNome());
            statement.setDate(3, usuario.getNascimento());
            statement.setString(4, usuario.getDescricao());
            statement.setInt(5, usuario.getServidor());
           // statement.executeUpdate();

            try (ResultSet result = statement.executeQuery()) {
                if(result.next()){
                usuario.setId(result.getInt(1));
                }

            }
        } catch (SQLException ex) {
            throw ex;
        }
    }

    @Override
    public Usuario read(Integer id) throws SQLException {
        Usuario usuario = new Usuario();

        try (PreparedStatement statement = connection.prepareStatement(readQuery);) {
            statement.setInt(1, id);
            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {
                    usuario.setId(result.getInt("id"));
                    usuario.setLogin(result.getString("login"));
                    usuario.setNome(result.getString("nome"));
                    usuario.setNascimento(result.getDate("nascimento"));
                    usuario.setDescricao(result.getString("descricao"));

                    try {
                        usuario.setFoto(copiarStream(result.getBinaryStream("foto")));
                    } catch (IOException ex) {
                        Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    usuario.setTipofoto(result.getString("tipofoto"));

                } else {
                    throw new SQLException("Falha ao visualizar: usuário não encontrado.");
                }
            }
        }

        return usuario;
    }

    private static InputStream copiarStream(InputStream input) throws IOException {
        if (input == null) {
            return null;
        }
        try {
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            byte[] buffer = new byte[2048];
            while (true) {
                int bytes = input.read(buffer);
                if (bytes == -1) {
                    break;
                }

                output.write(buffer, 0, bytes);
            }
            return new ByteArrayInputStream(output.toByteArray());
        } finally {
            input.close();
        }
    }

    @Override
    public void update(Usuario usuario) throws SQLException {
        String query;

        if (usuario.getSenha() == null && usuario.getFoto() == null) {
            query = updateQuery;
        } else if (usuario.getSenha() == null) {
            query = updateWithFoto;
        } else if (usuario.getFoto() == null) {
            query = upadateWithPassowrd;
        } else {
            query = updateWithFotoPasswordQuery;
        }

        try (PreparedStatement statement = connection.prepareStatement(query);) {
            statement.setString(1, usuario.getLogin());
            statement.setString(2, usuario.getNome());
            statement.setDate(3, usuario.getNascimento());
            statement.setString(4, usuario.getDescricao());
            if (usuario.getSenha() == null && usuario.getFoto() == null) {
                statement.setInt(5, usuario.getId());
            } else if (usuario.getSenha() == null) {
                statement.setString(5, usuario.getTipofoto());
                statement.setBinaryStream(6, usuario.getFoto(), usuario.getTamanho());
                statement.setInt(7, usuario.getId());
            } else if (usuario.getFoto() == null) {
                statement.setString(5, usuario.getSenha());
                statement.setInt(6, usuario.getId());
            } else {
                statement.setString(5, usuario.getTipofoto());
                statement.setBinaryStream(6, usuario.getFoto(), usuario.getTamanho());
                statement.setString(7, usuario.getSenha());
                statement.setInt(8, usuario.getId());
            }

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Falha ao editar: usuário não encontrado.");
            }
        }
    }

    @Override
    public void delete(Integer id) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(deleteQuery);) {
            statement.setInt(1, id);

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Falha ao excluir: usuário não encontrado.");
            }
        }
    }

    public void deletaSeguir(Integer id_usuario1, Integer id_usuario2) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(delataseguidoresQuery);) {
            statement.setInt(1, id_usuario1);
            statement.setInt(2, id_usuario2);
            // statement.setBoolean(3, false);

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Relação não encontrada");
            }
        }
    }

    @Override
    public List<Usuario> all() throws SQLException {
        List<Usuario> usuarioList = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(allQuery);
                ResultSet result = statement.executeQuery();) {
            while (result.next()) {
                Usuario usuario = new Usuario();
                usuario.setId(result.getInt("id"));
                usuario.setLogin(result.getString("login"));
                usuario.setNome(result.getString("nome"));
                usuario.setDescricao(result.getString("descricao"));
                usuario.setNascimento(result.getDate("nascimento"));

                usuarioList.add(usuario);
            }
        } catch (SQLException ex) {
            throw ex;
        }

        return usuarioList;
    }
    
    public List<Usuario> allimportado() throws SQLException {
        List<Usuario> usuarioList = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(allMinhasQuery);
                ResultSet result = statement.executeQuery();) {
            while (result.next()) {
                Usuario usuario = new Usuario();
                usuario.setId(result.getInt("id"));
                usuario.setLogin(result.getString("login"));
                usuario.setNome(result.getString("nome"));
                usuario.setDescricao(result.getString("descricao"));
                usuario.setNascimento(result.getDate("nascimento"));

                usuarioList.add(usuario);
            }
        } catch (SQLException ex) {
            throw ex;
        }

        return usuarioList;
    }

    public List<Usuario> busca(Usuario u) throws SQLException {
        List<Usuario> usuarioList = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(buscaQuery);) {
            statement.setString(1, "%" + u.getNome() + "%");
            try (ResultSet result = statement.executeQuery();) {
                while (result.next()) {
                    Usuario usuario = new Usuario();
                    usuario.setId(result.getInt("id"));
                    usuario.setLogin(result.getString("login"));
                    usuario.setNome(result.getString("nome"));

                    usuarioList.add(usuario);
                }
            } catch (SQLException ex) {
                throw ex;
            }
        }

        return usuarioList;
    }

    public void seguir(Integer id_usuario1, Integer id_usuario2) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(seguidoresQuery);) {

            statement.setInt(1, id_usuario1);
            statement.setInt(2, id_usuario2);

            statement.executeUpdate();
        } catch (SQLException ex) {
            throw ex;
        }

    }

    public boolean verificaSeguidor(Integer id_usuario1, Integer id_usuario2) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(verificaSeguidor);) {
            statement.setInt(1, id_usuario1);
            statement.setInt(2, id_usuario2);
            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {
                    return true;
                }
            }
            return false;

        }
    }

    public void authenticate(Usuario usuario) throws SQLException, SecurityException {
        try (PreparedStatement statement = connection.prepareStatement(authenticateQuery);) {
            statement.setString(1, usuario.getLogin());

            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {
                    MessageDigest md5;

                    String senhaForm;
                    String senhaUsuario;

                    try {
                        md5 = MessageDigest.getInstance("MD5");
                        md5.update(usuario.getSenha().getBytes());

                        senhaForm = new BigInteger(1, md5.digest()).toString(16);
                        senhaUsuario = result.getString("senha");

                        if (!senhaForm.equals(senhaUsuario)) {
                            throw new SecurityException("Senha incorreta.");
                        }
                    } catch (NoSuchAlgorithmException ex) {
                        System.err.println(ex.getMessage());
                    }

                    usuario.setId(result.getInt("id"));
                    usuario.setNome(result.getString("nome"));
                    usuario.setNascimento(result.getDate("nascimento"));
                } else {
                    throw new SecurityException("Login incorreto.");
                }
            }
        } catch (SQLException ex) {
            throw ex;
        }
    }
}
