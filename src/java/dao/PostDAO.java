package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import model.Comentario;
import model.Dislikes;
import model.Likes;
import model.Post;
import model.Repost;
import model.Usuario;

public class PostDAO extends DAO<Post> {

    public PostDAO(Connection connection) {
        super(connection);
    }
    private static final String createQuery = "INSERT INTO post(id_usuario, texto, data) VALUES (?,?, CURRENT_TIMESTAMP) RETURNING id_post;";

    private static final String createImportadoQuery = "INSERT INTO post(id_usuario, texto, data,titulo, servidor) VALUES (?,?, CURRENT_TIMESTAMP,?,?) RETURNING id_post;";
    private static final String readQuery = "SELECT * FROM  post WHERE id_post = (?) ORDER BY data DESC;";
    private static final String updateQuery = "UPDATE post SET texto = ? WHERE id_post = (?);";
    private static final String deletePostsQuery = "DELETE FROM post WHERE id_usuario = (?);";
    private static final String deletePostQuery = "DELETE FROM post WHERE id_post = (?);";
    private static final String allQuery = "SELECT * FROM post ORDER BY data DESC;";
    private static final String allImportadoQuery = "SELECT * FROM post  WHERE servidor is NULL ORDER BY data DESC;";
    private static final String todosPosts = "SELECT * FROM post WHERE id_usuario = (?) ORDER BY data DESC;";

    //apaga tudo usuario
    private static final String apagaTudoQuery = "DELETE FROM usuario WHERE servidor=?;";
    //retorna posts de seguidores
    private static final String postsSeguidoresQuery = "SELECT texto,nome, id_post, id_usuario, data FROM seguidores JOIN post ON id_usuario1 = id_usuario JOIN usuario ON id_usuario1 = id WHERE  id_usuario2 = (?) ORDER BY data DESC;";
    //query para comentarios
    private static final String insereComentarioQuery = "INSERT INTO comentarios(id_post,id_usuario, texto , data) VALUES (?,?,?, CURRENT_TIMESTAMP);";
    private static final String insereComentarioImportadoQuery = "INSERT INTO comentarios(id_post,id_usuario, texto, data, servidor) VALUES (?,?,?, CURRENT_TIMESTAMP,?);";
    private static final String deletaComentarioQuery = "DELETE FROM comentarios WHERE id_comentario = (?);";
    private static final String retornaComentarioQuery = "SELECT texto,nome,id_comentario, id_usuario FROM comentarios JOIN usuario ON id_usuario =id WHERE id_post = (?)";
    private static final String allComentarioQuery = "SELECT * FROM comentarios WHERE servidor is NULL ORDER BY data DESC ;";
    private static final String retornaComentarioPostQuery = "SELECT e.texto, e.id_post, b.nome FROM comentarios e JOIN post a  ON e.id_post = a.id_post JOIN usuario b ON e.id_usuario = id WHERE a.id_post = (?);";

    //query para repostagem
    private static final String insereRepostagem = "INSERT INTO repostagem(id_post, id_usuario, data) VALUES (?,?, CURRENT_TIMESTAMP);";

    private static final String insereRepostagemImportada = "INSERT INTO repostagem(id_post, id_usuario, data, servidor) VALUES (?,?, CURRENT_TIMESTAMP,?);";
    private static final String retornaRepostagem = "SELECT a.texto, a.id_post, a.data FROM post a JOIN repostagem b ON a.id_post = b.id_post WHERE b.id_usuario =(?)  ORDER BY a.data DESC;";
    private static final String allRepost = "SELECT * FROM repostagem  WHERE servidor is NULL ORDER BY data DESC;";

    //query para like e dislike
    private static final String insereLike = "INSERT INTO likes(id_usuario, id_post, data) VALUES (?,?, CURRENT_TIMESTAMP);";

    private static final String insereLikeImportado = "INSERT INTO likes(id_usuario, id_post, data, servidor) VALUES (?,?, CURRENT_TIMESTAMP,?);";
    private static final String insereDislike = "INSERT INTO dislikes(id_usuario, id_post, data) VALUES (?,?, CURRENT_TIMESTAMP);";
    private static final String insereDislikeImportado = "INSERT INTO dislikes(id_usuario, id_post, data, servidor) VALUES (?,?, CURRENT_TIMESTAMP, ?);";
    private static final String retiraLike = "DELETE FROM likes WHERE id_usuario =(?) AND id_post = (?);";
    private static final String retiraDislike = "DELETE FROM dislikes WHERE id_usuario =(?) AND id_post = (?);";
    private static final String verificaLike = "SELECT * FROM likes WHERE id_usuario = (?) AND id_post = (?);";
    private static final String verificaDislike = "SELECT * FROM dislikes WHERE id_usuario = (?) AND id_post =(?);";
    private static final String qtdLikes = "SELECT COUNT (*) AS qtd FROM likes WHERE id_post = (?);";
    private static final String qtdDislikes = "SELECT COUNT (*) AS qtd FROM dislikes WHERE id_post = (?);";
    private static final String verificaEmail = "SELECT * FROM usuario WHERE login=(?);";
    private static final String verificaGrupo = "SELECT * FROM grupo WHERE tag=(?);";
    private static final String allLike = "SELECT * FROM likes WHERE servidor is NULL ORDER BY data DESC ;";
    private static final String allDislike = "SELECT * FROM dislikes WHERE servidor is NULL ORDER BY data DESC ;";
    //querys para hashtag
    private static final String inserirTagQuery = "INSERT INTO hashtag (id_post, hashtag) VALUES (?,?);";
    private static final String deleteTagQuery = "DELETE FROM hashtag WHERE id_post =(?);";
    private static final String retornaPorTag = "SELECT * FROM post b JOIN hashtag a ON a.id_post = b.id_post WHERE a.hashtag = (?);";

    //query para estatisticas
    
    //consulta para pegar os seguidores e consulta pra pegar quem segue os seguidores, faz uniao e conta o resultado
    private static final String influenciaQuery = "SELECT count(DISTINCT(k.id_usuario2)) as influencia FROM ((SELECT s.id_usuario2 FROM seguidores s WHERE id_usuario1 = ?) UNION (SELECT s.id_usuario2 FROM seguidores s WHERE id_usuario1 IN (SELECT s.id_usuario2 FROM seguidores s WHERE id_usuario1 = ?))) as k;";

    private static final String top20UsuariosQuery = "SELECT u.*,"
            + " ((2 * (SELECT count(*) FROM repostagem r JOIN post p ON p.id_post = r.id_post WHERE p.id_usuario = u.id AND r.data  BETWEEN ? AND ?))"
            + " + ((2 * (SELECT count(*) FROM comentarios c JOIN post p ON p.id_post = c.id_post WHERE p.id_post = u.id AND c.data  BETWEEN ? AND ?)) "
            + "+ (SELECT count(*) FROM likes l JOIN post p ON p.id_post = l.id_post WHERE p.id_post = u.id AND l.data  BETWEEN ? AND ?))"
            + " - (SELECT count(*) FROM dislikes d JOIN post p ON p.id_post = d.id_post WHERE p.id_post = u.id AND d.data BETWEEN ? AND ?))"
            + " AS top FROM usuario u ORDER BY top DESC LIMIT 20;";

    private static final String top3UsuariosQuery = "SELECT u.*,"
            + " ((2 * (SELECT count(*) FROM repostagem r JOIN post p ON p.id_post = r.id_post WHERE p.id_usuario = u.id AND r.data BETWEEN ? AND ?))"
            + " + (2 * (SELECT count(*) FROM comentarios c JOIN post p ON p.id_post = c.id_post WHERE p.id_post = u.id AND c.data BETWEEN ? AND ?))"
            + " + (SELECT count(*) FROM likes l JOIN post p ON p.id_post = l.id_post WHERE p.id_post = u.id AND l.data BETWEEN ? AND ? )"
            + " - (SELECT count(*) FROM dislikes d JOIN post p ON p.id_post = d.id_post WHERE p.id_post = u.id AND d.data BETWEEN ? AND ?)) "
            + "AS top FROM usuario u ORDER BY top DESC LIMIT 3;";

    private static final String top20postsQuery = "SELECT p.*, u.nome, "
            + "((3 * (SELECT count(*) FROM repostagem r WHERE r.id_post = p.id_post AND data  BETWEEN ? AND ?))+ "
            + "(3 * (SELECT count(*) FROM comentarios c WHERE c.id_post = p.id_post AND data  BETWEEN ? AND ?))+ "
            + "(2 * (SELECT count(*) FROM likes l WHERE l.id_post = p.id_post AND data  BETWEEN ? AND ?))+ "
            + "(SELECT count(*) FROM dislikes d WHERE d.id_post = p.id_post AND data  BETWEEN ? AND ?)) "
            + "AS loko "
            + "FROM post p JOIN usuario u ON p.id_usuario = u.id GROUP BY p.id_post, u.nome ORDER by loko DESC LIMIT 20;";
    
    
    // posts comentados por usuario 1 e 2, posts repostados por 1 e 2, dividido pela quantidade de post do 1 e a mesma coisa com reposts.
    private static final String similaridadeQuery = "SELECT *,\n"
            + "            (((SELECT count(p.id_post)::float FROM post p JOIN repostagem r ON (r.id_post = p.id_post AND r.id_usuario = ?) JOIN repostagem k ON (k.id_post = p.id_post AND k.id_usuario = u2.id))\n"
            + "           /(SELECT CASE count(*)::float  WHEN 0 THEN 1 ELSE count(*) END FROM repostagem r, post p WHERE r.id_post = p.id_post AND p.id_usuario = ?))\n"
            + "            +(SELECT count(p.id_post)::float  FROM post p JOIN comentarios c1 ON (c1.id_post = p.id_post AND c1.id_usuario= ?) JOIN comentarios c2 ON (c2.id_post = p.id_post AND c2.id_usuario = u2.id))\n"
            + "          /(SELECT CASE count(*)::float  WHEN 0 THEN 1 ELSE count(*) END FROM comentarios c,post p WHERE c.id_post= p.id_post AND p.id_usuario = ?))/2 AS Rank\n"
            + "           FROM usuario u2 WHERE u2.id != ?  ORDER BY Rank DESC  LIMIT 10;";

    @Override

    public void create(Post post) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(createQuery);) {

            statement.setInt(1, post.getId_usuario());
            statement.setString(2, post.getTexto());

            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {

                    post.setId_post(result.getInt("id_post"));

                } else {
                    throw new SQLException("BOOM!");
                }
            }

        } catch (SQLException ex) {
            throw ex;
        }
    }

    public void createImportado(Post post) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(createImportadoQuery);) {

            statement.setInt(1, post.getId_usuario());
            statement.setString(2, post.getTexto());
            statement.setString(3, post.getTitulo());
            statement.setInt(4, post.getServidor());

            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {

                    post.setId_post(result.getInt("id_post"));

                } else {
                    throw new SQLException("BOOM!");
                }
            }

        } catch (SQLException ex) {
            throw ex;
        }
    }

    @Override
    public Post read(Integer id_post) throws SQLException {
        Post post = new Post();

        try (PreparedStatement statement = connection.prepareStatement(readQuery);) {
            statement.setInt(1, id_post);

            //
            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {

                    post.setId_post(result.getInt("id_post"));
                    post.setId_usuario(result.getInt("id_usuario"));
                    post.setTexto(result.getString("texto"));
                    post.setData(result.getTimestamp("data"));

                } else {
                    throw new SQLException("Falha ao visualizar: Post não encontrado.");
                }
            }
        }
        return post;
    }

    @Override
    public void update(Post post) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(updateQuery);) {
            statement.setString(1, post.getTexto());

            statement.setInt(2, post.getId_post());

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Falha ao editar: Post não encontrado.");
            }

        }
    }

    @Override
    public void delete(Integer id_post) throws SQLException {

        try (PreparedStatement statement = connection.prepareStatement(deletePostQuery);) {
            statement.setInt(1, id_post);

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Falha ao excluir: usuário não encontrado.");
            }
        }
    }

    public void deletarTudo(Integer id_servidor) throws SQLException {

       
        try (PreparedStatement statement = connection.prepareStatement(apagaTudoQuery);) {
           
            statement.setInt(1, id_servidor);
            statement.executeUpdate();
           
        }
    }

    /*//Sobrescrita
     public void delete(Integer id, Post post) throws SQLException {
     String query;
     if (Objects.equals(post.getId_usuario(), id)) {
     query = deletePostsQuery;
     } else {
     query = deletePostQuery;
     }
     try (PreparedStatement statement = connection.prepareStatement(query);) {
     statement.setInt(1, id);

     if (statement.executeUpdate() < 1) {
     throw new SQLException("Falha ao excluir");
     }
     }
     } */
    public void criaComentario(Integer id_post, Integer id_usuario, String texto) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(insereComentarioQuery);) {

            statement.setInt(1, id_post);
            statement.setInt(2, id_usuario);
            statement.setString(3, texto);

            statement.executeUpdate();
        } catch (SQLException ex) {
            throw ex;
        }
    }

    public void criaComentario(Comentario comentario) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(insereComentarioImportadoQuery);) {

            statement.setInt(1, comentario.getId_post());
            statement.setInt(2, comentario.getId_usuario());
            statement.setString(3, comentario.getConteudo());
            statement.setInt(4, comentario.getServidor());

            statement.executeUpdate();
        } catch (SQLException ex) {
            throw ex;
        }
    }

    public void deletaComentario(Integer id_comentario) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(deletaComentarioQuery);) {
            statement.setInt(1, id_comentario);

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Comentario não encontrado");
            }
        }

    }

    public void repostar(Integer id_post, Integer id_usuario) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(insereRepostagem);) {

            statement.setInt(1, id_post);
            statement.setInt(2, id_usuario);

            statement.executeUpdate();
        } catch (SQLException ex) {
            throw ex;
        }

    }

    public void repostar(Repost repost) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(insereRepostagemImportada);) {

            statement.setInt(1, repost.getId_post());
            statement.setInt(2, repost.getId_usuario());
            statement.setInt(3, repost.getServidor());

            statement.executeUpdate();
        } catch (SQLException ex) {
            throw ex;
        }

    }

    public void like(Integer id_usuario, Integer id_post) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(insereLike);) {

            statement.setInt(1, id_usuario);
            statement.setInt(2, id_post);

            statement.executeUpdate();
        } catch (SQLException ex) {
            throw ex;
        }

    }

    public void like(Likes like) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(insereLikeImportado);) {

            statement.setInt(1, like.getId_usuario());
            statement.setInt(2, like.getId_post());
            statement.setInt(3, like.getServidor());

            statement.executeUpdate();
        } catch (SQLException ex) {
            throw ex;
        }

    }

    public void dislike(Integer id_usuario, Integer id_post) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(insereDislike);) {

            statement.setInt(1, id_usuario);
            statement.setInt(2, id_post);

            statement.executeUpdate();
        } catch (SQLException ex) {
            throw ex;
        }
    }

    public void dislike(Dislikes dislike) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(insereDislikeImportado);) {

            statement.setInt(1, dislike.getId_usuario());
            statement.setInt(2, dislike.getId_post());
            statement.setInt(3, dislike.getServidor());

            statement.executeUpdate();
        } catch (SQLException ex) {
            throw ex;
        }
    }

    public Integer qtdLike(Integer id_post) throws SQLException {
        Integer a = null;
        try (PreparedStatement statement = connection.prepareStatement(qtdLikes);) {
            statement.setInt(1, id_post);

            try (ResultSet result = statement.executeQuery();) {

                while (result.next()) {
                    a = result.getInt("qtd");

                }
            } catch (SQLException ex) {
                throw ex;
            }
        }
        return a;
    }

    public void insereHashtag(Integer id_post, String hashtag) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(inserirTagQuery);) {

            statement.setInt(1, id_post);
            statement.setString(2, hashtag);

            statement.executeUpdate();
        } catch (SQLException ex) {
            throw ex;
        }

    }

    public void deletaHashtag(Integer id_post) throws SQLException {

        try (PreparedStatement statement = connection.prepareStatement(deleteTagQuery);) {
            statement.setInt(1, id_post);

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Relacao não encontrada");
            }
        }

    }

    public Integer qtdDislike(Integer id_post) throws SQLException {
        Integer b = null;
        try (PreparedStatement statement = connection.prepareStatement(qtdDislikes);) {
            statement.setInt(1, id_post);

            try (ResultSet result = statement.executeQuery();) {

                while (result.next()) {
                    b = result.getInt("qtd");

                }
            } catch (SQLException ex) {
                throw ex;
            }
        }
        return b;
    }

    public void removeLike(Integer id_usuario, Integer id_post) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(retiraLike);) {
            statement.setInt(1, id_usuario);
            statement.setInt(2, id_post);

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Relacao não encontrada");
            }
        }

    }

    public void removeDislike(Integer id_usuario, Integer id_post) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(retiraDislike);) {
            statement.setInt(1, id_usuario);
            statement.setInt(2, id_post);

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Relacao não encontrada");
            }
        }

    }

    public boolean verificaLike(Integer id_usuario, Integer id_post) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(verificaLike);) {
            statement.setInt(1, id_usuario);
            statement.setInt(2, id_post);
            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {
                    return true;
                }
            }
            return false;

        }
    }

    public boolean verificaDislike(Integer id_usuario, Integer id_post) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(verificaDislike);) {
            statement.setInt(1, id_usuario);
            statement.setInt(2, id_post);
            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {
                    return true;
                }
            }
            return false;

        }
    }

    public boolean verificaLogin(String login) throws SQLException {

        boolean retorno = false;
        try (PreparedStatement statement = connection.prepareStatement(verificaEmail);) {
            statement.setString(1, login);
            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {
                    retorno = true;
                }
            }

        }

        return retorno;

    }

    public boolean verificaGrupo(String tag) throws SQLException {

        boolean retorno = false;
        try (PreparedStatement statement = connection.prepareStatement(verificaGrupo);) {
            statement.setString(1, tag);
            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {
                    retorno = true;
                }
            }

        }

        return retorno;

    }

    public Integer retornaId(String login) throws SQLException {
        Integer retorno = null;

        try (PreparedStatement statement = connection.prepareStatement(verificaEmail);) {
            statement.setString(1, login);
            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {
                    retorno = result.getInt("id");
                }
            }
        }
        return retorno;
    }

    public Integer retornaGrupo(String tag) throws SQLException {
        Integer retorno = null;

        try (PreparedStatement statement = connection.prepareStatement(verificaGrupo);) {
            statement.setString(1, tag);
            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {
                    retorno = result.getInt("id_grupo");
                }
            }
        }
        return retorno;
    }

    public Post retornaPostcomTag(Post p, Usuario u, Integer id_logado) throws SQLException {

        String texto = p.getTexto();
        //p.setId_usuario(u.getId());
        p.setTexto(retornaComentario(texto, id_logado));

        return p;
    }

    public List<Post> busca(List<String> arrobas, List<String> hashtags, Integer id_logado) throws SQLException {
        List<Post> postagens = new ArrayList();

        if (arrobas.isEmpty() && hashtags.isEmpty()) {
            return this.all();

        }
        String query = "SELECT DISTINCT ON (b.id_post) b.id_post, b.texto, u.nome FROM post b"
                + " JOIN usuario u ON u.id = b.id_usuario"
                + " LEFT JOIN hashtag a ON a.id_post = b.id_post"
                + " JOIN seguidores ON id_usuario2 = (?) AND id_usuario1 = b.id_usuario"
                + " LEFT JOIN membros m ON u.id = m.id_usuario"
                + " LEFT JOIN grupo g ON m.id_grupo = g.id_grupo"
                + " WHERE ";

        if (arrobas.size() > 0) {
            query += "(";
            for (int i = 0; i < arrobas.size(); i++) {
                query += "(login = ? OR g.tag = ?)";

                if (i != (arrobas.size() - 1)) {
                    query += " OR ";

                }

            }
            query += ")";
        }
        if (arrobas.size() > 0 && hashtags.size() > 0) {

            query += " AND ";
        }

        if (hashtags.size() > 0) {
            query += "(";
            for (int i = 0; i < hashtags.size(); i++) {

                query += "a.hashtag =(?)";

                if (i != (hashtags.size() - 1)) {
                    query += " OR ";

                }

            }
            query += ")";
        }
        query += ";";

        // System.out.println(query);
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            int index = 1;
            statement.setInt(index, id_logado);
            index++;
            for (int i = 0; i < arrobas.size(); i++) {

                statement.setString(index, arrobas.get(i));
                index++;
                statement.setString(index, arrobas.get(i));
                index++;
            }
            for (int i = 0; i < hashtags.size(); i++) {
                statement.setString(index, hashtags.get(i));
                index++;
            }
            try (ResultSet result = statement.executeQuery();) {
                while (result.next()) {
                    Post post = new Post();

                    post.setId_post(result.getInt("id_post"));

                    post.setTexto(result.getString("texto"));
                    post.setNome(result.getString("nome"));

                    postagens.add(post);
                }

            }

            return postagens;
        }
    }

    private String retornaComentario(String texto, Integer id_logado) throws SQLException {
        String[] split;
        try (DAOFactory daoFactory = new DAOFactory();) {

            PostDAO dao = daoFactory.getPostDAO();
            split = texto.split(" ");
            int idusuario;
            int idgrupo;
            for (int i = 0; i < split.length; i++) {
                if (split[i].startsWith("@")) {
                    String teste = split[i].substring(1);
                    if (dao.verificaLogin(teste)) {
                        idusuario = dao.retornaId(teste);
                        split[i] = "<a href=\"/bd2014/usuario/read?id=" + idusuario + "&id_usuario2=" + id_logado + "\">" + split[i] + "</a>";
                    } else if (dao.verificaGrupo(teste)) {
                        idgrupo = dao.retornaGrupo(teste);
                        split[i] = "<a href=\"/bd2014/grupo/read?id_grupo=" + idgrupo + "\">" + split[i] + "</a>";
                    }
                }
                if (split[i].startsWith("#")) {
                    String hashtag = split[i].substring(1);
                    split[i] = "<a href=\"/bd2014/post/posthashtag?hashtag=" + hashtag + "\">" + split[i] + "</a>";
                }

                if (split[i].startsWith("$l:\"") && split[i].endsWith("\"")) {
                    String link = split[i].substring(4, split[i].length() - 1);
                    split[i] = "<a href=\"" + link + "\">" + link + "</a>";
                }
                if (split[i].startsWith("$i:\"") && split[i].endsWith("\"")) {
                    String link = split[i].substring(4, split[i].length() - 1);
                    split[i] = "<img style=\"max-height:150px\" src=\"" + link + "\" >";
                }
                if (split[i].startsWith("$v:\"") && split[i].endsWith("\"")) {
                    String link = split[i].substring(4, split[i].length() - 1);
                    split[i] = "<video width=\"320\" height=\"240\" controls> <source src=\"" + link + "\" type=\"video/webm\"> </video>";
                }
            }
            String conteudo = "";
            for (int i = 0; i < split.length; i++) {
                conteudo = conteudo + " " + split[i];
            }
            return conteudo;
        }
    }

    public Post retornaComentariocomArroba(Post p, Usuario u, Integer id_logado) throws SQLException {
        String texto = p.getComentario();
        String[] split;
        // p.setId_usuario(u.getId());
        p.setComentario(retornaComentario(texto, id_logado));
        return p;
    }

    public List<Post> retornaPostsHashtag(String hashtag) throws SQLException {
        List<Post> retornaPostsHashtag = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(retornaPorTag);) {
            statement.setString(1, hashtag);
            try (ResultSet result = statement.executeQuery();) {
                while (result.next()) {
                    Post post = new Post();

                    post.setId_post(result.getInt("id_post"));
                    post.setId_usuario(result.getInt("id_usuario"));
                    post.setTexto(result.getString("texto"));
                    post.setData(result.getTimestamp("data"));

                    retornaPostsHashtag.add(post);
                }
            }
        } catch (SQLException ex) {
            throw ex;
        }

        return retornaPostsHashtag;
    }

    @Override
    public List<Post> all() throws SQLException {
        List<Post> postList = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(allQuery);
                ResultSet result = statement.executeQuery();) {
            while (result.next()) {
                Post post = new Post();

                post.setId_post(result.getInt("id_post"));
                post.setId_usuario(result.getInt("id_usuario"));
                post.setTexto(result.getString("texto"));
                post.setData(result.getTimestamp("data"));
                post.setTitulo(result.getString("titulo"));

                postList.add(post);
            }
        } catch (SQLException ex) {
            throw ex;
        }
        return postList;
    }

    public List<Post> allimportado() throws SQLException {
        List<Post> postList = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(allImportadoQuery);
                ResultSet result = statement.executeQuery();) {
            while (result.next()) {
                Post post = new Post();

                post.setId_post(result.getInt("id_post"));
                post.setId_usuario(result.getInt("id_usuario"));
                post.setTexto(result.getString("texto"));
                post.setData(result.getTimestamp("data"));
                post.setTitulo(result.getString("titulo"));

                postList.add(post);
            }
        } catch (SQLException ex) {
            throw ex;
        }

        return postList;
    }

    public List<Repost> allrepost() throws SQLException {
        List<Repost> repostList = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(allRepost);
                ResultSet result = statement.executeQuery();) {
            while (result.next()) {
                Repost repost = new Repost();

                repost.setId_post(result.getInt("id_post"));
                repost.setId_usuario(result.getInt("id_usuario"));
                repost.setId(result.getInt("id"));
                repost.setData(result.getTimestamp("data"));

                repostList.add(repost);
            }
        } catch (SQLException ex) {
            throw ex;
        }

        return repostList;
    }

    public List<Comentario> allComentario() throws SQLException {
        List<Comentario> comentarioList = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(allComentarioQuery);
                ResultSet result = statement.executeQuery();) {
            while (result.next()) {
                Comentario comentario = new Comentario();

                comentario.setId(result.getInt("id_comentario"));
                comentario.setId_post(result.getInt("id_post"));
                comentario.setId_usuario(result.getInt("id_usuario"));
                comentario.setConteudo(result.getString("texto"));
                comentario.setData(result.getTimestamp("data"));

                comentarioList.add(comentario);
            }
        } catch (SQLException ex) {
            throw ex;
        }

        return comentarioList;
    }

    public List<Likes> allLikes() throws SQLException {
        List<Likes> likesList = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(allLike);
                ResultSet result = statement.executeQuery();) {
            while (result.next()) {
                Likes like = new Likes();

                like.setId_post(result.getInt("id_post"));
                like.setId_usuario(result.getInt("id_usuario"));

                like.setData(result.getTimestamp("data"));

                likesList.add(like);
            }
        } catch (SQLException ex) {
            throw ex;
        }

        return likesList;
    }

    public List<Dislikes> allDislikes() throws SQLException {
        List<Dislikes> dislikesList = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(allDislike);
                ResultSet result = statement.executeQuery();) {
            while (result.next()) {
                Dislikes dislike = new Dislikes();

                dislike.setId_post(result.getInt("id_post"));
                dislike.setId_usuario(result.getInt("id_usuario"));

                dislike.setData(result.getTimestamp("data"));

                dislikesList.add(dislike);
            }
        } catch (SQLException ex) {
            throw ex;
        }

        return dislikesList;
    }

    public List<Post> getTodosPosts(Integer id) throws SQLException {
        List<Post> postsUsuario = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(todosPosts);) {
            statement.setInt(1, id);
            try (ResultSet result = statement.executeQuery();) {
                while (result.next()) {
                    Post post = new Post();

                    post.setId_post(result.getInt("id_post"));
                    post.setId_usuario(result.getInt("id_usuario"));
                    post.setTexto(result.getString("texto"));
                    post.setData(result.getTimestamp("data"));

                    postsUsuario.add(post);
                }
            }
        } catch (SQLException ex) {
            throw ex;
        }

        return postsUsuario;
    }

    public List<Post> listaComentario(Integer id_post) throws SQLException {
        List<Post> listaComentario = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(retornaComentarioQuery);) {
            statement.setInt(1, id_post);
            try (ResultSet result = statement.executeQuery();) {
                while (result.next()) {
                    Post post = new Post();

                    post.setComentario(result.getString("texto"));
                    post.setNome(result.getString("nome"));
                    post.setId_comentario(result.getInt("id_comentario"));
                    post.setId_usuario(result.getInt("id_usuario"));
                    listaComentario.add(post);
                }
            }
        } catch (SQLException ex) {
            throw ex;
        }

        return listaComentario;
    }

    public List<Post> listaComentariosMeusPosts(Integer id_post) throws SQLException {
        List<Post> listaComentariomeus = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(retornaComentarioPostQuery);) {
            statement.setInt(1, id_post);
            try (ResultSet result = statement.executeQuery();) {
                while (result.next()) {
                    Post post = new Post();

                    post.setComentario(result.getString("texto"));
                    post.setId_post(result.getInt("id_post"));
                    post.setNome(result.getString("nome"));

                    listaComentariomeus.add(post);
                }
            }
        } catch (SQLException ex) {
            throw ex;
        }

        return listaComentariomeus;
    }

    public List<Post> listaRepostagem(Integer id_usuario) throws SQLException {
        List<Post> listaRepostagem = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(retornaRepostagem);) {
            statement.setInt(1, id_usuario);
            try (ResultSet result = statement.executeQuery();) {
                while (result.next()) {
                    Post post = new Post();

                    post.setTexto("Repostagem ->  " + result.getString("texto"));
                    post.setId_post(result.getInt("id_post"));
                    post.setData(result.getTimestamp("data"));

                    listaRepostagem.add(post);
                }
            }
        } catch (SQLException ex) {
            throw ex;
        }

        return listaRepostagem;
    }

    //public List<Object> getPostsSeguidores(Integer id) throws SQLException {
    public List<Post> getPostsSeguidores(Integer id) throws SQLException {

        List<Post> postsSeguidores = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(postsSeguidoresQuery);) {
            statement.setInt(1, id);
            try (ResultSet result = statement.executeQuery();) {
                while (result.next()) {
                    Post post = new Post();

                    /*postsSeguidores.set(i, result.getString("texto"));
                     postsSeguidores.set(i+1, result.getString("nome"));
                     i++;*/
                    post.setTexto(result.getString("texto"));
                    post.setTextoOriginal(result.getString("texto"));
                    post.setNome(result.getString("nome"));
                    post.setId_post(result.getInt("id_post"));
                    post.setId_usuario(result.getInt("id_usuario"));
                    post.setData(result.getTimestamp("data"));


                    /*  
                     postsSeguidores.add(result.getString("texto"));
                     postsSeguidores.add(result.getString("nome")); */
                    postsSeguidores.add(post);

                }
            }
        } catch (SQLException ex) {
            throw ex;
        }

        return postsSeguidores;
    }

    public List<Usuario> top20usuario(Timestamp data_inicio, Timestamp data_fim) throws SQLException {
        List<Usuario> usuarioList = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(top20UsuariosQuery)) {
            for (int i = 0; i < 4; i++) {
                statement.setTimestamp((i * 2) + 1, data_inicio);
                statement.setTimestamp((i * 2) + 2, data_fim);
            }

            try (ResultSet result = statement.executeQuery();) {
                while (result.next()) {
                    Usuario usuario = new Usuario();
                    usuario.setId(result.getInt("id"));
                    usuario.setLogin(result.getString("login"));
                    usuario.setNome(result.getString("nome"));
                    usuario.setPontuacao(result.getInt("top"));

                    usuarioList.add(usuario);
                }
            }
        } catch (SQLException ex) {
            throw ex;
        }

        return usuarioList;
    }

    public List<Usuario> top3usuario(Timestamp data_inicio, Timestamp data_fim) throws SQLException {
        List<Usuario> usuarioList = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(top3UsuariosQuery)) {
            for (int i = 0; i < 4; i++) {
                statement.setTimestamp((i * 2) + 1, data_inicio);
                statement.setTimestamp((i * 2) + 2, data_fim);
            }

            try (ResultSet result = statement.executeQuery();) {
                while (result.next()) {
                    Usuario usuario = new Usuario();
                    usuario.setId(result.getInt("id"));
                    usuario.setLogin(result.getString("login"));
                    usuario.setNome(result.getString("nome"));
                    usuario.setPontuacao(result.getInt("top"));

                    usuarioList.add(usuario);
                }
            } catch (SQLException ex) {
                throw ex;
            }

            return usuarioList;
        }
    }

    public List<Post> top20postagens(Timestamp data_inicio, Timestamp data_fim) throws SQLException {
        List<Post> postsUsuario = new ArrayList<>();
        //  System.out.println("Data inicio" + data_inicio + "data fim:" + data_fim);
        try (PreparedStatement statement = connection.prepareStatement(top20postsQuery)) {
            for (int i = 0; i < 4; i++) {
                statement.setTimestamp((i * 2) + 1, data_inicio);
                statement.setTimestamp((i * 2) + 2, data_fim);
            }

            try (ResultSet result = statement.executeQuery();) {
                while (result.next()) {
                    Post post = new Post();

                    post.setId_post(result.getInt("id_post"));
                    post.setId_usuario(result.getInt("id_usuario"));
                    post.setTexto(result.getString("texto"));
                    post.setNome(result.getString("nome"));
                    int loko = result.getInt("loko");

                    post.setPontuacao(loko);
                    //  System.out.println(loko);

                    postsUsuario.add(post);
                }
            }
        } catch (SQLException ex) {
            throw ex;
        }

        return postsUsuario;
    }

    public Integer zona_influencia(Integer id) throws SQLException {

        Integer retorno = null;
        try (PreparedStatement statement = connection.prepareStatement(influenciaQuery)) {
            statement.setInt(1, id);
            statement.setInt(2, id);

            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {
                    retorno = result.getInt("influencia");
                }
            }
        }
        return retorno;
    }

    public List<Usuario> Similaridade(Integer id) throws SQLException {
        List<Usuario> usuarioList = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(similaridadeQuery)) {
            statement.setInt(1, id);
            statement.setInt(2, id);
            statement.setInt(3, id);
            statement.setInt(4, id);
            statement.setInt(5, id);

            try (ResultSet result = statement.executeQuery();) {
                while (result.next()) {
                    Usuario usuario = new Usuario();

                    usuario.setNome(result.getString("nome"));
                    usuario.setInfluencia(result.getFloat("Rank"));

                    usuarioList.add(usuario);
                }
            }
        } catch (SQLException ex) {
            throw ex;
        }

        return usuarioList;
    }

}
