<%-- 
    Document   : index
    Created on : Aug 8, 2014, 10:17:16 PM
    Author     : Kelvin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>[BD 2014] Postagens </title>
        <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>

        <form action="${pageContext.servletContext.contextPath}/post/delete" method="POST">
            <table  class="table">
                <thead>
                    <tr>
                        <th>Texto</th>
                        <th>ID_post</th>
                        <th>ID</th>
                        <th class ="text-center">Ação</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="p" items="${postList}">
                       
                            <tr>
                                <td>${p.texto}</td>
                                <td><c:out value="${p.id_post}"/></td>
                                <td>
                                    <a href="${pageContext.servletContext.contextPath}/post/read?id_post=${p.id_post}">
                                        <c:out value="${p.id_usuario}"/>
                                    </a>
                                </td>
                                 <c:if test="${p.id_usuario == autenticado}">
                                <td class ="text-center">
                                    <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/post/update?id_post=${p.id_post}">Editar</a>
                                
                                
                                    <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/post/delete?id_post=${p.id_post}">Excluir</a>
                                </td>
                                <td>
                                    <input type="checkbox" name="delete" value="${p.id_post}">
                                </td>
                                </c:if>
                            </tr>
                        
                    </c:forEach>
                </tbody>
            </table>
            <input type="submit" class="btn btn-default" value="Excluir múltiplas postagens">

        </form>

        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Navegar</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.servletContext.contextPath}/">Voltar</a></li>


                    </ul>
                </div>
            </div>
        </nav>

    </body>
</html>
