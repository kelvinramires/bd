<%-- 
    Document   : comentario
    Created on : Aug 27, 2014, 8:53:35 PM
    Author     : Kelvin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>[BD 2014] Comentarios do Post </title>
        <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <h2>Comentario da postagem</h2>



        <c:forEach var="c" items="${listaComentario}" varStatus="loop">

            Comentario ${loop.index}: ${c.comentario}<br>                
            Autor : <c:out value="${c.nome}"/><br>
             <c:if test="${c.id_usuario == autenticado}">
            <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/post/apagacoment?id_comentario=${c.id_comentario}">Apagar comentario</a>
            </c:if>
            
            <br><br>



        </c:forEach>







        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Navegar</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.servletContext.contextPath}/">Voltar</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/">Home page</a></li>


                    </ul>
                </div>
            </div>
        </nav>

    </body>
</html>