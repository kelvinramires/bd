<%-- 
    Document   : estatisticas
    Created on : Oct 8, 2014, 6:11:29 PM
    Author     : Kelvin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>[BD 2014]Estatisticas</title>
        <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <h1>Tipos de Estatisticas</h1>



        <h3>Pesquisar Top 20 Postagens</h3>

        <form action="${pageContext.servletContext.contextPath}/post/top20" method ="GET">

            <label>Data de Inicio:</label>
            <input type="datetime-local" name="data_inicio"><br><br>
            <label>Data de Fim:</label>
            <input type="datetime-local" name="data_fim"><br><br>
            <input type="submit" class="btn btn-default" value="Enviar">
        </form>

        <h3>Pesquisar Top 20 Usuarios</h3>

        <form action="${pageContext.servletContext.contextPath}/post/topusu" method ="GET">

            <label>Data de Inicio:</label>
            <input type="datetime-local" name="data_inicio"><br><br>
            <label>Data de Fim:</label>
            <input type="datetime-local" name="data_fim"><br><br>
            <input type="submit" class="btn btn-default" value="Enviar">
        </form>

        <h3>Pesquisar Top 3 Usuarios + Grafico</h3>

        <form action="${pageContext.servletContext.contextPath}/post/top3" method ="GET">

            <label>Dia Escolhido:</label>
            <input type="date" name="data"><br><br>
            <input type="submit" class="btn btn-default" value="Enviar">
        </form>

        <h3>Pesquisar por Similaridade</h3>
        <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/post/similaridade?id=${usuario}">Similaridade</a>







        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Navegar</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.servletContext.contextPath}/post">Voltar</a></li>


                    </ul>
                </div>
            </div>
        </nav>
    </body>
</html>
