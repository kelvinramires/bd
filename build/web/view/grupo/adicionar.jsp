<%-- 
    Document   : adicionar
    Created on : Aug 10, 2014, 6:54:22 PM
    Author     : Kelvin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>[BD 2014] Adicionar Usuarios no grupo</title>
        <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet">

    </head>
    <body>
        <h1>Lista de usuários</h1>


        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Login</th>
                    <th colspan="2">Ação</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="u" items="${usuarioList}">
                    <tr>
                        <td><c:out value="${u.id}"/></td>
                        <td>
                            <c:out value="${u.login}"/>
                        </td>
                        <td>
                            <form action="${pageContext.servletContext.contextPath}/grupo/adicionar" method="POST">
                                <input type="hidden" name="id_usuario" value="${u.id}">
                                <input type="hidden" name="id_grupo" value="${id_grupo}">
                                <input type="submit" value="Adicionar"> 
                            </form>

                        </td>

                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Navegar</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.servletContext.contextPath}/grupo">Voltar</a></li>


                    </ul>
                </div>
            </div>
        </nav>
    </body>
</html>
