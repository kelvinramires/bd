<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <title>[BD 2014] Início</title>

        <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.servletContext.contextPath}/css/bootstrap.css" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">


            <nav class="navbar navbar-default " role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">Menus</a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="${pageContext.servletContext.contextPath}/usuario">Usuários</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/post/timeline?id=${usuario.id}">Timeline</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/grupo">Grupos</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/grupo/create"> Criar um Grupo</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/post/estatisticas"> Estatisticas </a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/importar"> Importar </a></li>
                        </ul>
                        <form class="navbar-form navbar-left" role="search" action="${pageContext.servletContext.contextPath}/post/busca" method="GET">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Busca @ e #" name="busca">
                            </div>
                            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-tree-deciduous"></span>Filtra</button>
                        </form>

                        <form class="navbar-form navbar-left" role="search" action="${pageContext.servletContext.contextPath}/usuario/buscausuario" method="GET">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Busca usuario" name="nome">
                            </div>
                            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-tree-conifer"></span>Busca</button>
                        </form>


                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
        </div>


        <h1 class="text-center">Bem-vindo, <c:out value="${usuario.nome}"/>!</h1>






        <h2>Suas informações</h2>

        <ul>
            <c:if test="${not empty usuario.foto}">

                <li><img style="max-height:150px" src="${pageContext.servletContext.contextPath}/usuario/foto?id=${usuario.id}"></li>


            </c:if>
            <li>ID: <c:out value="${usuario.id}"/></li>
            <li>Login: <c:out value="${usuario.login}"/></li>

            <li>Nome: <c:out value="${usuario.nome}"/></li>
            <li>Descrição: <c:out value="${usuario.descricao}"/></li>
            <li>Data de nascimento: <c:out value="${usuario.nascimento}"/></li>
            
            <li>Sua Zona de Influencia é: <c:out value="${retorno}"/></li>

        </ul>

        <button type="button" class="btn btn-default"><a href="${pageContext.servletContext.contextPath}/usuario/update?id=${usuario.id}">Editar</a></button>
        <button type="button" class="btn btn-default"><a href="${pageContext.servletContext.contextPath}/usuario/delete?id=${usuario.id}">Excluir</a></button>


        <h2>Suas Postagens</h2>
        <ul>   
            <c:forEach var="xpto" items="${posts}" varStatus="loop">
                <c:set var="p" value="${xpto.post}" />

                <li>Post ${loop.index}: "${p.texto}" </li>
                <li>     Data:<c:out value="${p.data}"/>  </li>
            
                <li>    Quantidade de Likes : <c:out value="${xpto.qtd_Like}"/>   </li>
                <li>    Quantidade de Dislikes:<c:out value="${xpto.qtd_Dislike}"/> </li>
                    <c:set var="idpost" value="${p.id_post}" scope="session"/>
                <li><a class="btn btn-default" href="${pageContext.servletContext.contextPath}/post/comentariomeus?id_post=${p.id_post}"> Visualizar Comentarios</a></li>

            </c:forEach>
            <c:forEach var="c" items="${listaRepostagem}" varStatus="loop">
                <li>Post: <c:out value="${c.texto}"/></li>
                </c:forEach>  

        </ul>
        <h3> Sorte de hoje: "Izaias esta ao seu lado" </h3>
        <div class="container-fluid">


            <nav class="navbar navbar-default" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">Posts</a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="${pageContext.servletContext.contextPath}/post">Edição de Postagens</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/post/create"> Nova Postagem</a></li>


                        </ul>
                    </div>
                </div>
        </div>


    </body>

</html>