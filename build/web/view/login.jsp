<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
   <head>
       <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet">
      <title>[BD 2014] Login</title>
      
   </head>
   <body>
      <div class="container">
          <div class="row">
              <div class="col-md-6 col-md-offset-3">
      <form  class="form-horizontal" role="form" action="${pageContext.servletContext.contextPath}/login" method="POST">
         <div class="form-group">
            <label for="inputLogin" class="col-sm-2 control-label">Login:</label><br>
            <div class="col-sm-10">
               <input type="text" class="form-control" id="inputLogin" placeholder="Login" name ="login">
            </div>
         </div>
         <!-- <input type="text" name="login"><br><br> -->
         <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">Senha:</label>
            <div class="col-sm-10">
               <input type="password" class="form-control" id="inputPassword3" placeholder="Senha" name="senha">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
               <div class="checkbox">
                  <label>
                  <input type="checkbox"> Remember me
                  </label>
               </div>
            </div>
         </div>
         <!-- <input type="password" name="senha"><br><br> -->
         <div class="form-group">
         <div class="col-sm-offset-2 col-sm-10">
         <input type="submit" value="Login">
         <h4><a href="${pageContext.servletContext.contextPath}/usuario/create">Cadastrar usuário</a></h4>
      </form>
      </div>
      </div>
      </div>
   </body>
</html>
