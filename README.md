# REDE SOCIAL DE PUBLICAÇÕES#



O tema do estudo de caso da disciplina de 5COP092 - Bancos de Dados, turma 2014, é uma rede social de publicações. Em resumo, é uma forma de interação e comunicação entre usuários a partir de mensagens postadas no sistema.


**Ciclo 1 :**

a) gerenciamento de usuários (cada usuário pode cadastrar-se/logar no sistema/editar seus dados/excluir-se);

-- os dados devem incluir, ao menos: login, senha, nome de exibição, foto, descrição e data de nascimento.

b) criação de grupos (cada usuário pode criar/editar/remover grupos e associar/desassociar usuários a estes grupos);

c) criação/edição/remoção de posts (somente texto, nesse ciclo) por usuários autenticados;

d) páginas de perfil de usuário, contendo seus dados e os posts que publicou;

-- deve haver uma página de "perfil" que mostre, para o usuário atualmente autenticado, os seus posts;

-- deve haver formas de listar os demais usuários cadastrados e acessar os seus "perfis" (dados e posts);


**Ciclo 2:** 

a) Acompanhamento de publicações: um usuário pode selecionar usuários para acompanhar as suas publicações;

-- essa é a segunda visão de publicações citada anteriormente, que mostra as publicações dos usuários a quem o usuário logado segue;

-- uma "terceira" visão de publicações é a visão das publicações do usuário logado ("minhas pubicações"), que é a sua página de perfil;

-- o sistema deve habilitar a ação de seguir (ou deixar de seguir) um usuário a partir do perfil desse usuário;

-- o sistema deve ter uma forma de efetuar buscas de usuários, a partir dos seus dados de cadastro; o resultado da busca deve ter links para seus respectivos perfis;

b) Filtrar, republicar e opiniar ((undo)like/(undo)dislike) a respeito de publicações: seguindo a especificação indicada;

c) As tags das mensagens deverão ser tratadas nesse ciclo.