<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <title>[BD 2014] Usuários</title>
        <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.servletContext.contextPath}/css/bootstrap.css" rel="stylesheet">
    </head>
    <body>
        <h1>Visualização do perfil de  <c:out value="${usuario.nome}"/></h1>
        <c:if test="${usuario.id != autenticado}">
         <c:if test ="${not teste}">        
        
        <a class="btn btn-default" role="button" href="${pageContext.servletContext.contextPath}/usuario/seguir?id_usuario1=${usuario.id}&id_usuario2=${autenticado}">Seguir</a>
        </c:if>        
       <c:if test ="${teste}">
        <a class="btn btn-default" role="button" href="${pageContext.servletContext.contextPath}/usuario/naoseguir?id_usuario1=${usuario.id}&id_usuario2=${autenticado}">Deixar de Seguir</a>
        </c:if>
        </c:if>
        <ul>
            <li>ID: <c:out value="${usuario.id}"/></li>
            <li>Login: <c:out value="${usuario.login}"/></li>
            <li>Nome: <c:out value="${usuario.nome}"/></li>
            <li>A Zona de influencia é: <c:out value="${retorno}"/></li>
            
                <c:if test="${not empty usuario.foto}">

                <li> Foto: <img style="max-height:150px" src="${pageContext.servletContext.contextPath}/usuario/foto?id=${usuario.id}"></li>


            </c:if>
            <li>Descrição: <c:out value="${usuario.descricao}"/></li>
            <li>Data de nascimento: <c:out value="${usuario.nascimento}"/></li>




        </ul>
        <h1>Postagens</h1>
        <ul>   
            <c:forEach var="p" items="${posts}" varStatus="loop">

                <li>Post ${loop.index}: ${p.texto}</li>

            </c:forEach>
        </ul> 
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Navegar</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.servletContext.contextPath}/usuario">Voltar</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/">Home page</a></li>


                    </ul>
                </div>
            </div>
        </nav>
       
        
    </body>
</html>