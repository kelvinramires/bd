<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <title>[BD 2014] Usuários Updade</title>
        <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <h1>Edição do usuário <c:out value="${usuario.nome}"/></h1>
 
        
        <form  action="${pageContext.servletContext.contextPath}/usuario/update" method="POST" enctype="multipart/form-data">
            <label>ID:</label><br>
            <input type="text" name="id_disabled" value="${usuario.id}" disabled><br><br>

            <label>Login:</label><br>
            <input type="text" name="login" value="${usuario.login}"><br><br>

            <label>Senha:</label><br>
            <input type="password" name="senha"><br><br>

            <label>Nome:</label><br>
            <input type="text" name="nome" value="${usuario.nome}"><br><br>

            <label>Data de nascimento:</label><br>
            <input type="date" name="nascimento" value="${usuario.nascimento}"><br><br>
            
            <label>Descrição:</label><br>
            <textarea name="descricao">${usuario.descricao}</textarea><br><br>
            <input type="hidden" name="id" value="${usuario.id}">
            <label>Foto:</label><br>
            <input type="file" name="foto"><br><br>

            
            
         <!--  <label>Foto:</label>
            <input type="file" name="foto"><br><br> -->
            

            

            <input type="submit" value="Enviar">
        </form>

         <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Navegar</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.servletContext.contextPath}/usuario">Voltar</a></li>


                    </ul>
                </div>
            </div>
        </nav>
       
    </body>
</html>