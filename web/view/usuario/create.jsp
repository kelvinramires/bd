<%-- 
    Document   : create
    Created on : Jul 28, 2014, 4:16:12 PM
    Author     : Kelvin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!--enctype="multipart/form-data -->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>[BD 2014] Usuários</title>
        <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <h1>Cadastro de Usuário</h1>
        <form action="${pageContext.servletContext.contextPath}/usuario/create" method ="POST">
            <label>Login:</label><br>
            <input type="text" name="login" required><br><br>
            <label>Senha:</label>
            <input type ="password" name="senha"><br><br>
            
            <label>Nome:</label>
            <input type="text" name="nome"><br><br>
            
            <label>Data Nascimento:</label>
            <input type="date" name="nascimento"><br><br>
            
            <label>Descrição:</label><br>
            <textarea name="descricao">Descreva suas caracteristicas, izaias-sama quer saber.</textarea><br><br>
            
              
         <!--  <label>Foto:</label>
            <input type="file" name="foto"><br><br> -->
            
            <input type="submit" value="Enviar">
            
        </form>
        
        <h3><a href="${pageContext.servletContext.contextPath}/usuario">Voltar</a></h1>
    </body>
</html>
