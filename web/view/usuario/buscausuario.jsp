<%-- 
    Document   : buscausuario
    Created on : Aug 31, 2014, 7:40:30 PM
    Author     : Kelvin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>[BD 2014] Busca</title>
        <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <h1>Retorno da Busca</h1>

        <c:forEach var="u" items="${busca}">

          
            Nome:
            <a href="${pageContext.servletContext.contextPath}/usuario/read?id=${u.id}&id_usuario2=${usuario}">
                <c:out value="${u.nome}"/>
            </a> <br><br>

        </c:forEach>


        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Navegar</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.servletContext.contextPath}/post">Voltar</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/">Home page</a></li>


                    </ul>
                </div>
            </div>
        </nav>



    </body>
</html>