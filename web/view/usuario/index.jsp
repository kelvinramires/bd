<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <title>[BD 2014] Usuários</title>
        <!-- Bootstrap -->
        <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <h1>Lista de usuários</h1>

        <form action="${pageContext.servletContext.contextPath}/usuario/delete" method="POST">
            <table  class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Login</th>
                        <th>Nome</th>

                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="u" items="${usuarioList}">
                        <tr>
                            <td><c:out value="${u.id}"/></td>
                            <td>
                                <a href="${pageContext.servletContext.contextPath}/usuario/read?id=${u.id}&id_usuario2=${usuario}">
                                    <c:out value="${u.login}"/>
                                </a>
                            </td>
                            <td>
                                <c:out value="${u.nome}"/>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>


        </form>

        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Navegar</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.servletContext.contextPath}/">Home page</a></li>


                    </ul>
                </div>
            </div>
        </nav>
       


                      
</body>
</html>