<%-- 
    Document   : index
    Created on : Aug 9, 2014, 10:41:04 PM
    Author     : Kelvin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>[BD 2014] Grupos </title>
        <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>



        <form action="${pageContext.servletContext.contextPath}/grupo/delete" method="POST">
            <table  class="table">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Descricao</th>
                        <th>ID_grupo</th>
                        <th>ID_Dono</th>
                        <th class="text-center">Ação</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="g" items="${grupoList}">
                        <tr>
                            <td><c:out value="${g.nome}"/></td>
                            <td><c:out value="${g.descricao}"/></td>
                            <td><c:out value="${g.id_grupo}"/></td>
                            <td>
                                <a href="${pageContext.servletContext.contextPath}/grupo/read?id_grupo=${g.id_grupo}">
                                    <c:out value="${g.id_usuario}"/>
                                </a>
                            </td>
                            <c:if test="${g.id_usuario == autenticado}">
                                <td class="text-center">

                                    <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/grupo/adicionar?id_grupo=${g.id_grupo}">Adicionar Usuarios no Grupo!</a>
                                
                                    <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/grupo/remover?id_grupo=${g.id_grupo}">Remover Usuarios no Grupo!</a>
                                
                                    <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/grupo/update?id_grupo=${g.id_grupo}">Editar</a>
                                
                                    <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/grupo/delete?id_grupo=${g.id_grupo}">Excluir</a>
                                </td>
                                <td>
                                    <input type="checkbox" name="delete" value="${g.id_grupo}">
                                </td>
                            </c:if>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <input type="submit" class="btn btn-default" value="Excluir múltiplos grupos">
        </form>

        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Navegar</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.servletContext.contextPath}/">Voltar</a></li>


                    </ul>
                </div>
            </div>
        </nav>

    </body>
</html>
