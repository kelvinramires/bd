<%-- 
    Document   : create
    Created on : Aug 9, 2014, 10:40:13 PM
    Author     : Kelvin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>[BD 2014]Novo Grupo </title>
        <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <h1>Criar novo Grupo</h1>
        
        <form action="${pageContext.servletContext.contextPath}/grupo/create" method ="POST">
            <label>Digite o Nome do Grupo</label>
            <input type="text" name="nome"><br><br>
            <label>Digite a Descricao do Grupo</label>
            <input type="text" name="descricao"><br><br>
            <label>Escolha uma tag ao grupo:</label>
            <input type="text" name="tag"><br><br>
            <input type="hidden" name="id_usuario" value="${usuario}">
            <input type="submit" value="Enviar">


        </form>
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Navegar</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.servletContext.contextPath}/grupo">Voltar</a></li>


                    </ul>
                </div>
            </div>
        </nav>
    </body>
</html>
