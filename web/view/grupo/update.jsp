<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>[BD 2014] Update Grupo</title>
        <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <h1>Edi��o do Grupo <c:out value="${grupo.id_grupo}"/></h1>

        <form action="${pageContext.servletContext.contextPath}/grupo/update" method="POST">

            <label>Digite o novo nome</label>
            <input type="text" name="nome" value="${grupo.nome}"><br><br>
            <label>Digite a nova descricao</label>
            <input type="text" name="descricao" value="${grupo.descricao}"><br><br>

            <input type="hidden" name="id_usuario" value="${usuario}">
            <input type="hidden" name="id_grupo" value="${grupo.id_grupo}">

            <input type="submit" value="Enviar">
        </form>

            <nav class="navbar navbar-default" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">Navegar</a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="${pageContext.servletContext.contextPath}/grupo">Voltar</a></li>


                        </ul>
                    </div>
                </div>
            </nav>
    </body>
</html>
