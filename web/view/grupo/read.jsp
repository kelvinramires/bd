<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>[BD 2014]Visualizar Grupo</title>
        <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <h1>Visualização do grupo</h1>
        <ul>
            <li>Id_grupo: <c:out value="${grupo.id_grupo}"/></li>
            <li>Id_usuario: <c:out value="${grupo.id_usuario}"/></li>
            <li>Nome do Grupo: <c:out value="${grupo.nome}"/></li>
            <li>Descricao: <c:out value="${grupo.descricao}"/></li>


        </ul>
        <h4> Usuarios Cadastrados neste grupo </h4>
        
        <c:forEach var="u" items="${UsuariosCadastradosList}">
            <ul>
                <li>Id:<c:out value="${u.id}"/> </li>
                <li>Nome: <c:out value="${u.nome}"/></li>
            </ul>
        </c:forEach>

        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Navegar</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.servletContext.contextPath}/grupo">Voltar</a></li>


                    </ul>
                </div>
            </div>
        </nav>

    </body>
</html>
