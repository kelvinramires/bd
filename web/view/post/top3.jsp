<%-- 
    Document   : top3
    Created on : Oct 8, 2014, 8:12:39 PM
    Author     : Kelvin
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>[BD 2014]Usuarios</title>
        <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <h1>Grafico dos TOP 3 USUARIOS!</h1>

        <canvas id="myChart" width="400" height="400"></canvas>


        <c:forEach var="p" items="${datasets}" varStatus="loop">

            <div style="color: ${p.strokeColor}">
                ${p.label}
            </div>

        </c:forEach>


        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Navegar</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.servletContext.contextPath}/post">Voltar</a></li>


                    </ul>
                </div>
            </div>
        </nav>
        <script src="${pageContext.servletContext.contextPath}/js/Chart.js"></script>
        <script>
            var data = ${grafico};
//            var data = {
//                labels: ["January", "February", "March", "April", "May", "June", "July"],
//                datasets: [
//                    {
//                        label: "My First dataset",
//                        fillColor: "rgba(220,220,220,0.2)",
//                        strokeColor: "rgba(220,220,220,1)",
//                        pointColor: "rgba(220,220,220,1)",
//                        pointStrokeColor: "#fff",
//                        pointHighlightFill: "#fff",
//                        pointHighlightStroke: "rgba(220,220,220,1)",
//                        data: [65, 59, 80, 81, 56, 55, 40]
//                    },
//                    {
//                        label: "My Second dataset",
//                        fillColor: "rgba(151,187,205,0.2)",
//                        strokeColor: "rgba(151,187,205,1)",
//                        pointColor: "rgba(151,187,205,1)",
//                        pointStrokeColor: "#fff",
//                        pointHighlightFill: "#fff",
//                        pointHighlightStroke: "rgba(151,187,205,1)",
//                        data: [28, 48, 40, 19, 86, 27, 90]
//                    }
//                ]
//            };


            var ctx = document.getElementById("myChart").getContext("2d");

            new Chart(ctx).Line(data);

        </script>
    </body>
</html>
