<%-- 
    Document   : similaridade
    Created on : Oct 8, 2014, 8:44:56 PM
    Author     : Kelvin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>[BD 2014]Usuarios</title>
        <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <h1>Similaridade entre você e um usuário</h1>

        <table  class="table">
            <thead>
                <tr>
                 
                    <th>Nome</th>
                    <th>Similaridade</th>

                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="u" items="${usuarioList}">

                    <tr>
                      
                        <td><c:out value="${u.nome}"/></td>
                        
                        <td><c:out value="${u.influencia}"/></td>

                    </tr>

                </c:forEach>
            </tbody>
        </table>

        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Navegar</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.servletContext.contextPath}/post">Voltar</a></li>


                    </ul>
                </div>
            </div>
        </nav>
    </body>
</html>
