<%-- 
    Document   : timeline
    Created on : Aug 25, 2014, 7:32:39 PM
    Author     : Kelvin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>[BD 2014] Timeline </title>
        <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <h2>Postagens de quem voce segue</h2>

        <c:forEach var="xpto" items="${seguidoresPosts}" varStatus="loop">
            <c:set var="p" value="${xpto.post}" />






            Autor: <c:out value="${p.nome}"/><br>
            Conteudo: ${p.texto} <br>
            Data : <c:out value="${p.data}"/><br>
            Quantidade de Likes : <c:out value="${xpto.qtd_Like}"/>  Quantidade de Dislikes:<c:out value="${xpto.qtd_Dislike}"/><br>


            <c:if test="${not xpto.like}">
                <a class="btn btn-default" role="button" href="${pageContext.servletContext.contextPath}/post/like?id_usuario=${autenticado}&id_post=${p.id_post}"><span class="glyphicon glyphicon-thumbs-up"></span>Like</a>
            </c:if>
            <c:if test="${xpto.like}">
                <a class="btn btn-default" role="button" href="${pageContext.servletContext.contextPath}/post/unlike?id_usuario=${autenticado}&id_post=${p.id_post}"><span class="glyphicon glyphicon-thumbs-down"></span>Unlike</a>
            </c:if>
            <c:if test="${not xpto.dislike}">
                <a class="btn btn-default" role="button" href="${pageContext.servletContext.contextPath}/post/dislike?id_usuario=${autenticado}&id_post=${p.id_post}"><span class="glyphicon glyphicon-thumbs-down"></span>Dislike</a>
            </c:if>
            <c:if test="${xpto.dislike}">
                <a class="btn btn-default" role="button" href="${pageContext.servletContext.contextPath}/post/undislike?id_usuario=${autenticado}&id_post=${p.id_post}"><span class="glyphicon glyphicon-thumbs-up"></span>Undislike</a>
            </c:if>

            <form  action="${pageContext.servletContext.contextPath}/post/comentario" method="POST">
                <input type="hidden" name="id_post" value="${p.id_post}">
                <input type="hidden" name="id_usuario" value="${autenticado}">
                <label>Insira um comentario:</label><br>
                <input type="text" name="texto"><br><br>
                <input class="btn btn-default" type="submit" value="Comentar">
                <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/post/comentario?id_post=${p.id_post}">Visualizar comentarios</a>
            </form>

            <form  action="${pageContext.servletContext.contextPath}/post/repost" method="POST">
                <input type="hidden" name="id_post" value="${p.id_post}">
                <input type="hidden" name="id_usuario" value="${autenticado}">
                <input type="hidden" name="texto" value="${p.textoOriginal}">
                <input class="btn btn-default" type="submit" value="Repostar">

                   <!-- <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/post/repost?id_post=${p.id_post}&id_usuario=${usuario}&texto=${p.texto}">Repostar</a> -->

            </form>

            <br><br>



        </c:forEach>





        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Navegar</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.servletContext.contextPath}/">Voltar</a></li>


                    </ul>
                </div>
            </div>
        </nav>

    </body>
</html>
