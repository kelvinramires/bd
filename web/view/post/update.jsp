<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>[BD 2014] Upadate Post</title>
        <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <h1>Edição do Post numero: <c:out value="${post.id_post}"/></h1>

        <form action="${pageContext.servletContext.contextPath}/post/update" method="POST">

            <label>Digite:</label>
            <input type="text" name="texto" value="<c:out value="${post.texto}"/>"><br><br>
          
            <input type="hidden" name="id_usuario" value="${autenticado}">
            <input type="hidden" name="id_post" value="${post.id_post}">

            <input type="submit" value="Enviar">
        </form>

        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Navegar</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.servletContext.contextPath}/post">Voltar</a></li>


                    </ul>
                </div>
            </div>
        </nav>



    </body>
</html>
