<%-- 
    Document   : comentariomeus
    Created on : Aug 28, 2014, 12:41:30 AM
    Author     : Kelvin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>[BD 2014] Comentarios nos seus Post </title>
        <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <h2>Comentario das suas postagem</h2>


       
        <c:forEach var="c" items="${listaComentario}" varStatus="loop">
           
            Comentario ${loop.index}: "${c.comentario}"<br>                
            Autor : <c:out value="${c.nome}"/><br>
            <c:set var="idposts" value="${c.id_post}" scope="page"/>
        
            
           
            
            <br><br>



        </c:forEach>
        
          <form  action="${pageContext.servletContext.contextPath}/post/comentario" method="POST">
                    <input type="hidden" name="id_post" value="${idposts}">
                    <input type="hidden" name="id_usuario" value="${autenticado}">
                    <label>Insira um comentario:</label><br>
                    <input type="text" name="texto"><br><br>
                    <input class="btn btn-default" type="submit" value="Comentar">
                    
             </form>  






        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Navegar</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.servletContext.contextPath}/">Voltar</a></li>


                    </ul>
                </div>
            </div>
        </nav>

    </body>
</html>