<%-- 
    Document   : top20
    Created on : Oct 8, 2014, 6:28:05 PM
    Author     : Kelvin
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>[BD 2014]Os 20 TOP POSTS</title>
        <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <h1>Lista de Postagens</h1>

        <table  class="table">
            <thead>
                <tr>
                    <th>Texto</th>
                    <th>Nome</th>
                    <th>ID_post</th>
                    <th>ID</th>
                    <th>Pontuacao</th>

                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="p" items="${postList}">

                    <tr>
                        <td>${p.texto}</td>
                        <td>${p.nome}</td>
                        <td><c:out value="${p.id_post}"/></td>
                        <td><c:out value="${p.id_usuario}"/></td>
                        <td><c:out value="${p.pontuacao}"/></td>

                    </tr>

                </c:forEach>
            </tbody>
        </table>

        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Navegar</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.servletContext.contextPath}/post">Voltar</a></li>


                    </ul>
                </div>
            </div>
        </nav>
    </body>
</html>
